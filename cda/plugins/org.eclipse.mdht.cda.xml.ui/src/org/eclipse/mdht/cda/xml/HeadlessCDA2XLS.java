package org.eclipse.mdht.cda.xml;

import java.util.Map;

import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;
import org.eclipse.mdht.cda.xml.ui.handlers.GenerateCDADataHandler;

public class HeadlessCDA2XLS implements IApplication {

	@Override
	public Object start(IApplicationContext context) throws Exception {

		final Map<?, ?> args = context.getArguments();
		final String[] appArgs = (String[]) args.get("application.args");
		String zipFile = "";
		for (final String arg : appArgs) {
			System.out.println(arg);
			if (arg.startsWith("-Z")) {
				zipFile = arg.replace("-Z", "");
			}
		}
		GenerateCDADataHandler generateCDADataHandler = new GenerateCDADataHandler();
		generateCDADataHandler.processCommandLine(zipFile);
		return IApplication.EXIT_OK;
	}

	@Override
	public void stop() {

	}

}
