package org.eclipse.mdht.cda.xml.ui.handlers;

import org.apache.poi.ss.usermodel.Row;

/**
 * @author seanmuir
 *
 */
public class SheetHeaderUtil {

	static boolean hideColumns = false;

	static String replaceSpace(String input) {
		String t = input.replaceAll(" ", "_");
		return t;
	}

	static int createAllergyHeader(Row row1, Row row2, int offset) {
		// All Des Verify Date Event Type Reaction Severity Source
		// int firstColumn = offset;
		// undo to go back to two rows for headers // undo to go back to two rows for headers
		// row1.createCell(firstColumn).setCellValue(replaceSpace("Allergy"));
		row2.createCell(offset++).setCellValue(replaceSpace(replaceSpace("Allergy ID")));
		row2.createCell(offset++).setCellValue(replaceSpace("Status"));
		row2.createCell(offset++).setCellValue(replaceSpace("Onset Date"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.getSheet().setColumnHidden(offset - 1, hideColumns);
		row2.createCell(offset++).setCellValue(replaceSpace("No Known Flag"));
		offset = addCodeHeader(row2, offset, "Allergy");
		if (GenerateCDADataHandler.exportCDTranslations) {
			offset = addExtendedCodeHeader(row2, offset, "Substance");
		} else {
			offset = addCodeHeader(row2, offset, "Substance");
		}

		offset = addCodeHeader(row2, offset, "Reaction");
		offset = addCodeHeader(row2, offset, "Severity");
		offset = addCodeHeader(row2, offset, "Criticality");
		row2.createCell(offset++).setCellValue(replaceSpace("Clinical Notes"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.getSheet().setColumnHidden(offset - 1, hideColumns);
		row2.createCell(offset++).setCellValue(replaceSpace("Organization"));
		row2.createCell(offset++).setCellValue(replaceSpace("Author"));
		offset = addSectionHeader(row2, offset);
		// undo to go back to two rows for headers // undo to go back to two rows for headers row1.getSheet().addMergedRegion(new CellRangeAddress(0,
		// 0, firstColumn, offset));

		return offset;
	}

	static int createDemographicsHeader(Row row1, Row row2, int offset) {
		row2.createCell(offset++).setCellValue(replaceSpace("Name"));
		row2.createCell(offset++).setCellValue(replaceSpace("Address"));
		row2.createCell(offset++).setCellValue(replaceSpace("City"));
		row2.createCell(offset++).setCellValue(replaceSpace("State"));
		row2.createCell(offset++).setCellValue(replaceSpace("Postal"));
		offset = addCodeHeader(row2, offset, "Race");
		offset = addCodeHeader(row2, offset, "Ethnicity");
		offset = addCodeHeader(row2, offset, "Gender");
		offset = addCodeHeader(row2, offset, "Marital Status");
		offset = addCodeHeader(row2, offset, "Langauge");
		row2.createCell(offset++).setCellValue(replaceSpace("Telephone"));
		row2.createCell(offset++).setCellValue(replaceSpace("File Name"));
		return offset;
	}

	/**
	 * @TODO
	 *       Added different columns and location for headers - make this user specified
	 * @param row2
	 * @param offset
	 * @return
	 */
	static int createDocumentMedadataHeadder(Row row2, int offset) {
		return createDocumentMedadataHeadder(row2, offset, true);
	}

	static int createDocumentMedadataHeadder(Row row2, int offset, boolean fileName) {
		if (fileName) {
			row2.createCell(offset++).setCellValue(replaceSpace("File Name"));
			row2.createCell(offset++).setCellValue(replaceSpace("Document ID"));
		}
		row2.createCell(offset++).setCellValue(replaceSpace("CDA Specification"));
		row2.createCell(offset++).setCellValue(replaceSpace("CDA Document Type"));
		row2.createCell(offset++).setCellValue(replaceSpace("Template Id"));
		row2.createCell(offset++).setCellValue(replaceSpace("CDA Document Code"));
		row2.createCell(offset++).setCellValue(replaceSpace("CDA Document System"));
		row2.createCell(offset++).setCellValue(replaceSpace("Organization"));
		row2.createCell(offset++).setCellValue(replaceSpace("Software"));
		row2.createCell(offset++).setCellValue(replaceSpace("Document Date"));
		row2.createCell(offset++).setCellValue(replaceSpace("PCP Name"));
		row2.createCell(offset++).setCellValue(replaceSpace("PCP Address"));
		return offset;
	}

	static int createEncounterHeader(Row row1, Row row2, int offset) {
		row2.createCell(offset++).setCellValue(replaceSpace("ID"));
		row2.createCell(offset++).setCellValue(replaceSpace("Date"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.getSheet().setColumnHidden(offset - 1, hideColumns);
		offset = addCodeHeader(row2, offset, "Encounter");
		offset = createProblemObservationHeader2(row1, row2, offset);
		row2.createCell(offset++).setCellValue(replaceSpace("Encounter Organization"));
		row2.createCell(offset++).setCellValue(replaceSpace("Encounter Author"));
		offset = addSectionHeader(row2, offset);

		return offset;
	}

	/**
	 * @param row1
	 * @param row2
	 * @param offset
	 * @return
	 */
	private static int createProblemObservationHeader2(Row row1, Row row2, int offset) {

		row2.createCell(offset++).setCellValue(replaceSpace("ID"));
		row2.createCell(offset++).setCellValue(replaceSpace("Date"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.getSheet().setColumnHidden(offset - 1, hideColumns);
		offset = addCodeHeader(row2, offset, "Problem");
		row2.createCell(offset++).setCellValue(replaceSpace("Problem Organization"));
		row2.createCell(offset++).setCellValue(replaceSpace("Problem Author"));
		// offset = addSectionHeader(row2, offset);
		return offset;

	}

	static int createEncounterIDHeader(Row row1, Row row2, int offset) {
		row2.createCell(offset++).setCellValue(replaceSpace("Encounter ID"));
		row2.createCell(offset++).setCellValue(replaceSpace("Encounter Match"));
		row2.getSheet().setColumnHidden(offset - 1, hideColumns);

		return offset;
	}

	static int createPatientHeader(Row row1, Row row2, int offset) {
		row2.createCell(offset++).setCellValue(replaceSpace("Record"));
		row2.createCell(offset++).setCellValue(replaceSpace("File Name"));
		row2.createCell(offset++).setCellValue(replaceSpace("Document ID"));
		row2.createCell(offset++).setCellValue(replaceSpace("Patient ID"));
		if (!GenerateCDADataHandler.omitDOB) {
			row2.createCell(offset++).setCellValue(replaceSpace("Complete ID"));
			row2.createCell(offset++).setCellValue(replaceSpace("Patient Name"));
			if (!"Documents".equals(row2.getSheet().getSheetName())) {
				row2.getSheet().setColumnHidden(offset - 1, hideColumns);
			}
			row2.createCell(offset++).setCellValue(replaceSpace("DOB"));
		}

		if (!GenerateCDADataHandler.omitDOD) {
			row2.createCell(offset++).setCellValue(replaceSpace("DOD"));
		}
		offset = createDocumentMedadataHeadder(row2, offset, false);
		return offset;
	}

	static int createPatientHeader2(Row row1, Row row2, int offset) {
		row2.createCell(offset++).setCellValue(replaceSpace("Recipient Name"));
		row2.createCell(offset++).setCellValue(replaceSpace("Recipient Organization"));
		row2.createCell(offset++).setCellValue(replaceSpace("Order Id"));
		row2.createCell(offset++).setCellValue(replaceSpace("File Name"));
		return offset;
	}

	static int createProblemHeader(Row row1, Row row2, int offset) {
		row2.createCell(offset++).setCellValue(replaceSpace("ID"));
		row2.createCell(offset++).setCellValue(replaceSpace("Date"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.createCell(offset++).setCellValue(replaceSpace("Status"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));

		row2.getSheet().setColumnHidden(offset - 1, hideColumns);
		offset = addCodeHeader(row2, offset, "Problem");

		offset = createProblemObservationHeader(row1, row2, offset);
		return offset;

	}

	static int createProblemObservationHeader(Row row1, Row row2, int offset) {
		row2.createCell(offset++).setCellValue(replaceSpace("ID"));
		row2.createCell(offset++).setCellValue(replaceSpace("Date"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.getSheet().setColumnHidden(offset - 1, hideColumns);
		if (GenerateCDADataHandler.exportCDTranslations) {
			offset = addExtendedCodeHeader(row2, offset, "Problem");
		} else {
			offset = addCodeHeader(row2, offset, "Problem");
		}

		row2.createCell(offset++).setCellValue(replaceSpace("Clinical Notes"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.getSheet().setColumnHidden(offset - 1, hideColumns);

		row2.createCell(offset++).setCellValue(replaceSpace("Problem Organization"));
		row2.createCell(offset++).setCellValue(replaceSpace("Problem Author"));
		offset = addSectionHeader(row2, offset);
		return offset;

	}

	/**
	 * @param row1
	 * @param row2
	 * @param offset
	 */
	static int createProcedureHeader(Row row1, Row row2, int offset) {
		row2.createCell(offset++).setCellValue(replaceSpace("ID"));
		row2.createCell(offset++).setCellValue(replaceSpace("Effective Date"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.createCell(offset++).setCellValue(replaceSpace("Start Date"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.createCell(offset++).setCellValue(replaceSpace("End Date"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.getSheet().setColumnHidden(offset - 1, hideColumns);
		if (GenerateCDADataHandler.exportCDTranslations) {
			offset = addExtendedCodeHeader(row2, offset, "Procedure");
		} else {
			offset = addCodeHeader(row2, offset, "Procedure");
		}
		row2.createCell(offset++).setCellValue(replaceSpace("Clinical Notes"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.getSheet().setColumnHidden(offset - 1, hideColumns);
		row2.createCell(offset++).setCellValue(replaceSpace("Performer"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.getSheet().setColumnHidden(offset - 1, hideColumns);
		row2.createCell(offset++).setCellValue(replaceSpace("Organization"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.getSheet().setColumnHidden(offset - 1, hideColumns);
		row2.createCell(offset++).setCellValue(replaceSpace("Section Title"));
		row2.createCell(offset++).setCellValue(replaceSpace("File Location"));
		return offset;

	}

	static int createResultsHeader(Row row1, Row row2, int offset) {
		row2.createCell(offset++).setCellValue(replaceSpace("Panel ID"));
		row2.createCell(offset++).setCellValue(replaceSpace("Date"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.getSheet().setColumnHidden(offset - 1, hideColumns);
		if (GenerateCDADataHandler.exportCDTranslations) {
			offset = addExtendedCodeHeader(row2, offset, "Panel");
		} else {
			offset = addCodeHeader(row2, offset, "Panel");
		}

		if (GenerateCDADataHandler.exportCDTranslations) {
			offset = addExtendedCodeHeader(row2, offset, "Specimen");
		} else {
			offset = addCodeHeader(row2, offset, "Specimen");
		}

		row2.createCell(offset++).setCellValue(replaceSpace("Organization"));
		row2.createCell(offset++).setCellValue(replaceSpace("Author"));
		row2.createCell(offset++).setCellValue(replaceSpace("ID"));
		row2.createCell(offset++).setCellValue(replaceSpace("Date"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.getSheet().setColumnHidden(offset - 1, hideColumns);
		row2.createCell(offset++).setCellValue(replaceSpace("Status"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.getSheet().setColumnHidden(offset - 1, hideColumns);
		if (GenerateCDADataHandler.exportCDTranslations) {
			offset = addExtendedCodeHeader(row2, offset, "Test");
		} else {
			offset = addCodeHeader(row2, offset, "Test");
		}
		row2.createCell(offset++).setCellValue(replaceSpace("Result"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.getSheet().setColumnHidden(offset - 1, hideColumns);
		row2.createCell(offset++).setCellValue(replaceSpace("Range"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.getSheet().setColumnHidden(offset - 1, hideColumns);
		row2.createCell(offset++).setCellValue(replaceSpace("Clinical Notes"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.getSheet().setColumnHidden(offset - 1, hideColumns);
		offset = addSectionHeader(row2, offset);
		return offset;
	}

	static int createObservationHeader(Row row1, Row row2, int offset) {
		row2.createCell(offset++).setCellValue(replaceSpace("ID"));
		row2.createCell(offset++).setCellValue(replaceSpace("Date"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.getSheet().setColumnHidden(offset - 1, hideColumns);
		offset = addCodeHeader(row2, offset, "Observation");
		row2.createCell(offset++).setCellValue(replaceSpace("Value"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.getSheet().setColumnHidden(offset - 1, hideColumns);
		offset = addSectionHeader(row2, offset);
		return offset;
	}

	static int createObservationHeaderWithClinicalNotes(Row row1, Row row2, int offset) {
		row2.createCell(offset++).setCellValue(replaceSpace("ID"));
		row2.createCell(offset++).setCellValue(replaceSpace("Date"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.getSheet().setColumnHidden(offset - 1, hideColumns);
		offset = addCodeHeader(row2, offset, "Observation");
		row2.createCell(offset++).setCellValue(replaceSpace("Value"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.getSheet().setColumnHidden(offset - 1, hideColumns);
		row2.createCell(offset++).setCellValue(replaceSpace("Clinical Notes"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.getSheet().setColumnHidden(offset - 1, hideColumns);
		offset = addSectionHeader(row2, offset);
		return offset;
	}

	static int createObservationHeaderWithAuthor(Row row1, Row row2, int offset) {
		row2.createCell(offset++).setCellValue(replaceSpace("ID"));
		row2.createCell(offset++).setCellValue(replaceSpace("Date"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.getSheet().setColumnHidden(offset - 1, hideColumns);

		if (GenerateCDADataHandler.exportCDTranslations) {
			offset = addExtendedCodeHeader(row2, offset, "Observation");
		} else {
			offset = addCodeHeader(row2, offset, "Observation");
		}

		row2.createCell(offset++).setCellValue(replaceSpace("Value"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.getSheet().setColumnHidden(offset - 1, hideColumns);
		row2.createCell(offset++).setCellValue(replaceSpace("Organization"));
		row2.createCell(offset++).setCellValue(replaceSpace("Author"));
		offset = addSectionHeader(row2, offset);
		return offset;
	}

	static int createAdvanceDirectivesHeader(Row row1, Row row2, int offset) {
		row2.createCell(offset++).setCellValue(replaceSpace("ID"));
		row2.createCell(offset++).setCellValue(replaceSpace("Date"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.getSheet().setColumnHidden(offset - 1, hideColumns);
		offset = addCodeHeader(row2, offset, "Advance Directive");
		row2.createCell(offset++).setCellValue(replaceSpace("Value"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.getSheet().setColumnHidden(offset - 1, hideColumns);
		offset = addSectionHeader(row2, offset);
		return offset;
	}

	static int createSubstanceAdministrationHeader(Row row1, Row row2, int offset, String type) {
		row2.createCell(offset++).setCellValue(replaceSpace("ID"));
		if (GenerateCDADataHandler.exportCDTranslations) {
			offset = addExtendedCodeHeader(row2, offset, type);
		} else {
			offset = addCodeHeader(row2, offset, type);
		}
		row2.createCell(offset++).setCellValue(replaceSpace("Status"));
		row2.createCell(offset++).setCellValue(replaceSpace("Quantity"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.getSheet().setColumnHidden(offset - 1, hideColumns);
		row2.createCell(offset++).setCellValue(replaceSpace("Administration Date"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.createCell(offset++).setCellValue(replaceSpace("Administration Start Date"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.createCell(offset++).setCellValue(replaceSpace("Administration End Date"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.createCell(offset++).setCellValue(replaceSpace("Lot Number"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.getSheet().setColumnHidden(offset - 1, hideColumns);
		offset = addCodeHeader(row2, offset, "Route Code");
		row2.createCell(offset++).setCellValue(replaceSpace("Prescription"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.getSheet().setColumnHidden(offset - 1, hideColumns);
		row2.createCell(offset++).setCellValue(replaceSpace("Clinical Notes"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.getSheet().setColumnHidden(offset - 1, hideColumns);
		row2.createCell(offset++).setCellValue(replaceSpace("Organization"));
		row2.createCell(offset++).setCellValue(replaceSpace("Author"));
		offset = addSectionHeader(row2, offset);
		return offset;
	}

	static int addCodeHeader(Row row1, int offset, String prefix) {
		row1.createCell(offset++).setCellValue(replaceSpace(prefix + " Text"));
		row1.createCell(offset++).setCellValue(replaceSpace(prefix + " Display Name"));
		row1.createCell(offset++).setCellValue(replaceSpace(prefix + " Code"));
		row1.getSheet().setColumnHidden(offset - 1, hideColumns);
		row1.createCell(offset++).setCellValue(replaceSpace(prefix + " Code System"));
		row1.getSheet().setColumnHidden(offset - 1, hideColumns);
		row1.createCell(offset++).setCellValue(replaceSpace(prefix + " Code System Name"));
		row1.getSheet().setColumnHidden(offset - 1, hideColumns);
		row1.createCell(offset++).setCellValue(replaceSpace(prefix + " Location"));
		row1.getSheet().setColumnHidden(offset - 1, hideColumns);
		return offset;
	}

	static int addExtendedCodeHeader(Row row1, int offset, String prefix) {
		row1.createCell(offset++).setCellValue(replaceSpace(prefix + " Text"));
		row1.createCell(offset++).setCellValue(replaceSpace(prefix + " Display Name"));
		row1.createCell(offset++).setCellValue(replaceSpace(prefix + " Code"));
		row1.getSheet().setColumnHidden(offset - 1, hideColumns);
		row1.createCell(offset++).setCellValue(replaceSpace(prefix + " Code System"));
		row1.getSheet().setColumnHidden(offset - 1, hideColumns);
		row1.createCell(offset++).setCellValue(replaceSpace(prefix + " Code System Name"));
		row1.getSheet().setColumnHidden(offset - 1, hideColumns);
		row1.createCell(offset++).setCellValue(replaceSpace(prefix + " Location"));
		row1.getSheet().setColumnHidden(offset - 1, hideColumns);

		row1.createCell(offset++).setCellValue(replaceSpace(prefix + " Translation 1 Text"));
		row1.createCell(offset++).setCellValue(replaceSpace(prefix + " Translation 1 Display Name"));
		row1.createCell(offset++).setCellValue(replaceSpace(prefix + " Translation 1 Code"));
		row1.getSheet().setColumnHidden(offset - 1, hideColumns);
		row1.createCell(offset++).setCellValue(replaceSpace(prefix + " Translation 1 Code System"));
		row1.getSheet().setColumnHidden(offset - 1, hideColumns);
		row1.createCell(offset++).setCellValue(replaceSpace(prefix + " Translation 1 Code System Name"));
		row1.getSheet().setColumnHidden(offset - 1, hideColumns);
		row1.createCell(offset++).setCellValue(replaceSpace(prefix + " Translation 1 Location"));
		row1.getSheet().setColumnHidden(offset - 1, hideColumns);

		row1.createCell(offset++).setCellValue(replaceSpace(prefix + " Translation 2 Text"));
		row1.createCell(offset++).setCellValue(replaceSpace(prefix + " Translation 2 Display Name"));
		row1.createCell(offset++).setCellValue(replaceSpace(prefix + " Translation 2 Code"));
		row1.getSheet().setColumnHidden(offset - 1, hideColumns);
		row1.createCell(offset++).setCellValue(replaceSpace(prefix + " Translation 2 Code System"));
		row1.getSheet().setColumnHidden(offset - 1, hideColumns);
		row1.createCell(offset++).setCellValue(replaceSpace(prefix + " Translation 2 Code System Name"));
		row1.getSheet().setColumnHidden(offset - 1, hideColumns);
		row1.createCell(offset++).setCellValue(replaceSpace(prefix + " Translation 2 Location"));
		row1.getSheet().setColumnHidden(offset - 1, hideColumns);

		row1.createCell(offset++).setCellValue(replaceSpace(prefix + " Translation 3 Text"));
		row1.createCell(offset++).setCellValue(replaceSpace(prefix + " Translation 3 Display Name"));
		row1.createCell(offset++).setCellValue(replaceSpace(prefix + " Translation 3 Code"));
		row1.getSheet().setColumnHidden(offset - 1, hideColumns);
		row1.createCell(offset++).setCellValue(replaceSpace(prefix + " Translation 3 Code System"));
		row1.getSheet().setColumnHidden(offset - 1, hideColumns);
		row1.createCell(offset++).setCellValue(replaceSpace(prefix + " Translation 3 Code System Name"));
		row1.getSheet().setColumnHidden(offset - 1, hideColumns);
		row1.createCell(offset++).setCellValue(replaceSpace(prefix + " Translation 3 Location"));
		row1.getSheet().setColumnHidden(offset - 1, hideColumns);

		return offset;
	}

	static int addSectionHeader(Row row1, int offset) {
		row1.createCell(offset++).setCellValue(replaceSpace("Section Title"));
		row1.createCell(offset++).setCellValue(replaceSpace("File Location"));
		row1.createCell(offset++).setCellValue(replaceSpace("Narrative"));

		return offset;
	}

	public static int createCarePlanHeader(Row row1, Row row2, int offset) {

		row2.createCell(offset++).setCellValue(replaceSpace("Panel ID"));
		row2.createCell(offset++).setCellValue(replaceSpace("Date"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.getSheet().setColumnHidden(offset - 1, hideColumns);
		offset = addCodeHeader(row2, offset, "Panel");
		row2.createCell(offset++).setCellValue(replaceSpace("Organization"));
		row2.createCell(offset++).setCellValue(replaceSpace("Author"));
		row2.createCell(offset++).setCellValue(replaceSpace("Vital Sign ID"));
		row2.createCell(offset++).setCellValue(replaceSpace("Date"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.getSheet().setColumnHidden(offset - 1, hideColumns);
		offset = addCodeHeader(row2, offset, "Vital Sign");
		row2.createCell(offset++).setCellValue(replaceSpace("Result"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.getSheet().setColumnHidden(offset - 1, hideColumns);
		row2.createCell(offset++).setCellValue(replaceSpace("Range"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.getSheet().setColumnHidden(offset - 1, hideColumns);
		offset = addSectionHeader(row2, offset);
		return offset;

	}

	/**
	 * @param row1
	 * @param row2
	 * @param offset
	 * @return
	 */
	public static int createClinicalStatmentHeader(Row row1, Row row2, int offset) {
		row2.createCell(offset++).setCellValue(replaceSpace("ID"));
		row2.createCell(offset++).setCellValue(replaceSpace("Date"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.getSheet().setColumnHidden(offset - 1, hideColumns);
		offset = addCodeHeader(row2, offset, "Code");
		row2.createCell(offset++).setCellValue(replaceSpace("Performer"));
		row2.createCell(offset++).setCellValue(replaceSpace("Organization"));
		row2.createCell(offset++).setCellValue(replaceSpace("Section Title"));
		row2.createCell(offset++).setCellValue(replaceSpace("File Location"));
		return offset;
	}

	public static int createFamilyHistoryHeader(Row row1, Row row2, int offset) {
		row2.createCell(offset++).setCellValue(replaceSpace("Organizer ID"));
		row2.createCell(offset++).setCellValue(replaceSpace("Date"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.getSheet().setColumnHidden(offset - 1, hideColumns);
		offset = addCodeHeader(row2, offset, "Description");
		row2.createCell(offset++).setCellValue(replaceSpace("Organization"));
		row2.createCell(offset++).setCellValue(replaceSpace("Author"));
		row2.createCell(offset++).setCellValue(replaceSpace("Observation ID"));
		row2.createCell(offset++).setCellValue(replaceSpace("Date"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.getSheet().setColumnHidden(offset - 1, hideColumns);
		offset = addCodeHeader(row2, offset, "Value");
		offset = addSectionHeader(row2, offset);
		return offset;
	}

	public static int createGoalsSectionHeader(Row row1, Row row2, int offset) {
		// row2.createCell(offset++).setCellValue(replaceSpace("Organizer ID"));
		// row2.createCell(offset++).setCellValue(replaceSpace("Date"));
		offset = addCodeHeader(row2, offset, "Description");
		row2.createCell(offset++).setCellValue(replaceSpace("Organization"));
		row2.createCell(offset++).setCellValue(replaceSpace("Author"));
		row2.createCell(offset++).setCellValue(replaceSpace("Observation ID"));
		row2.createCell(offset++).setCellValue(replaceSpace("Date"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.getSheet().setColumnHidden(offset - 1, hideColumns);
		offset = addCodeHeader(row2, offset, "Value");
		offset = addSectionHeader(row2, offset);
		return offset;
	}

	/**
	 * @param row1
	 * @param row2
	 * @param offset
	 */
	public static int createVitalSignsHeader(Row row1, Row row2, int offset) {

		row2.createCell(offset++).setCellValue(replaceSpace("Panel ID"));
		row2.createCell(offset++).setCellValue(replaceSpace("Date"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.getSheet().setColumnHidden(offset - 1, hideColumns);
		if (GenerateCDADataHandler.exportCDTranslations) {
			offset = addExtendedCodeHeader(row2, offset, "Panel");
		} else {
			offset = addCodeHeader(row2, offset, "Panel");
		}
		row2.createCell(offset++).setCellValue(replaceSpace("Organization"));
		row2.createCell(offset++).setCellValue(replaceSpace("Author"));
		row2.createCell(offset++).setCellValue(replaceSpace("Vital Sign ID"));
		row2.createCell(offset++).setCellValue(replaceSpace("Date"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.getSheet().setColumnHidden(offset - 1, hideColumns);
		row2.createCell(offset++).setCellValue(replaceSpace("Status"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.getSheet().setColumnHidden(offset - 1, hideColumns);

		if (GenerateCDADataHandler.exportCDTranslations) {
			offset = addExtendedCodeHeader(row2, offset, "Vital Sign");
		} else {
			offset = addCodeHeader(row2, offset, "Vital Sign");
		}

		row2.createCell(offset++).setCellValue(replaceSpace("Result"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.getSheet().setColumnHidden(offset - 1, hideColumns);
		row2.createCell(offset++).setCellValue(replaceSpace("Range"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.getSheet().setColumnHidden(offset - 1, hideColumns);
		row2.createCell(offset++).setCellValue(replaceSpace("Clinical Notes"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.getSheet().setColumnHidden(offset - 1, hideColumns);
		offset = addSectionHeader(row2, offset);
		return offset;

	}

	/**
	 * @param row1
	 * @param row2
	 * @param offset
	 * @return
	 */
	public static int createPlanOfCareActivityObservationHeader(Row row1, Row row2, int offset) {
		row2.createCell(offset++).setCellValue(replaceSpace("ID"));
		row2.createCell(offset++).setCellValue(replaceSpace("Date"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.getSheet().setColumnHidden(offset - 1, hideColumns);
		offset = addCodeHeader(row2, offset, "Observation");
		row2.createCell(offset++).setCellValue(replaceSpace("Value"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.getSheet().setColumnHidden(offset - 1, hideColumns);
		row2.createCell(offset++).setCellValue(replaceSpace("Range"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.getSheet().setColumnHidden(offset - 1, hideColumns);
		offset = addSectionHeader(row2, offset);
		return offset;
	}

	/**
	 * @param row1
	 * @param row2
	 * @param offset
	 * @return
	 */
	public static int createInstructionsHeader(Row row1, Row row2, int offset) {
		row2.createCell(offset++).setCellValue(replaceSpace("ID"));
		row2.createCell(offset++).setCellValue(replaceSpace("Date"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		row2.getSheet().setColumnHidden(offset - 1, hideColumns);
		offset = addCodeHeader(row2, offset, "Code");
		row2.createCell(offset++).setCellValue(replaceSpace("Performer"));
		row2.createCell(offset++).setCellValue(replaceSpace("Organization"));
		row2.createCell(offset++).setCellValue(replaceSpace("Section Title"));
		row2.createCell(offset++).setCellValue(replaceSpace("File Location"));
		return offset;
	}

	/**
	 * @param row1
	 * @param row2
	 * @param offset
	 * @return
	 */
	public static int createNarrativeHeader(Row row1, Row row2, int offset) {
		row2.createCell(offset++).setCellValue(replaceSpace("Narrative"));
		row2.createCell(offset++).setCellValue(replaceSpace("Section Title"));
		row2.createCell(offset++).setCellValue(replaceSpace("File Location"));
		return offset;
	}

	/**
	 * @param row1
	 * @param row2
	 * @param offset
	 * @return
	 */
	public static int createPayersHeader(Row row1, Row row2, int offset) {
		row2.createCell(offset++).setCellValue(replaceSpace("ID"));
		row2.createCell(offset++).setCellValue(replaceSpace("Date"));
		row2.createCell(offset++).setCellValue(replaceSpace("Location"));
		offset = addCodeHeader(row2, offset, "Policy Activity");
		row2.createCell(offset++).setCellValue(replaceSpace("Organization"));
		row2.createCell(offset++).setCellValue(replaceSpace("Author"));
		row2.createCell(offset++).setCellValue(replaceSpace("Payer Name"));
		row2.createCell(offset++).setCellValue(replaceSpace("Payer Organization"));
		row2.createCell(offset++).setCellValue(replaceSpace("Guarantor Name"));
		row2.createCell(offset++).setCellValue(replaceSpace("Guarantor Organization"));
		row2.createCell(offset++).setCellValue(replaceSpace("Coverage Name"));
		offset = addCodeHeader(row2, offset, "Coverage Relationship");
		row2.createCell(offset++).setCellValue(replaceSpace("Holder Name"));
		offset = addSectionHeader(row2, offset);
		return offset;
	}

}
