/**
 */
package org.eclipse.mdht.uml.hl7.datatypes;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IVL REAL</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.mdht.uml.hl7.datatypes.IVL_REAL#getLow <em>Low</em>}</li>
 *   <li>{@link org.eclipse.mdht.uml.hl7.datatypes.IVL_REAL#getCenter <em>Center</em>}</li>
 *   <li>{@link org.eclipse.mdht.uml.hl7.datatypes.IVL_REAL#getHigh <em>High</em>}</li>
 *   <li>{@link org.eclipse.mdht.uml.hl7.datatypes.IVL_REAL#getWidth <em>Width</em>}</li>
 * </ul>
 *
 * @see org.eclipse.mdht.uml.hl7.datatypes.DatatypesPackage#getIVL_REAL()
 * @model
 * @generated
 */
public interface IVL_REAL extends SXCM_REAL {
	/**
	 * Returns the value of the '<em><b>Low</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Low</em>' containment reference.
	 * @see #setLow(IVXB_REAL)
	 * @see org.eclipse.mdht.uml.hl7.datatypes.DatatypesPackage#getIVL_REAL_Low()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	IVXB_REAL getLow();

	/**
	 * Sets the value of the '{@link org.eclipse.mdht.uml.hl7.datatypes.IVL_REAL#getLow <em>Low</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Low</em>' containment reference.
	 * @see #getLow()
	 * @generated
	 */
	void setLow(IVXB_REAL value);

	/**
	 * Returns the value of the '<em><b>Center</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Center</em>' containment reference.
	 * @see #setCenter(REAL)
	 * @see org.eclipse.mdht.uml.hl7.datatypes.DatatypesPackage#getIVL_REAL_Center()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	REAL getCenter();

	/**
	 * Sets the value of the '{@link org.eclipse.mdht.uml.hl7.datatypes.IVL_REAL#getCenter <em>Center</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Center</em>' containment reference.
	 * @see #getCenter()
	 * @generated
	 */
	void setCenter(REAL value);

	/**
	 * Returns the value of the '<em><b>High</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>High</em>' containment reference.
	 * @see #setHigh(IVXB_REAL)
	 * @see org.eclipse.mdht.uml.hl7.datatypes.DatatypesPackage#getIVL_REAL_High()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	IVXB_REAL getHigh();

	/**
	 * Sets the value of the '{@link org.eclipse.mdht.uml.hl7.datatypes.IVL_REAL#getHigh <em>High</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>High</em>' containment reference.
	 * @see #getHigh()
	 * @generated
	 */
	void setHigh(IVXB_REAL value);

	/**
	 * Returns the value of the '<em><b>Width</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Width</em>' containment reference.
	 * @see #setWidth(REAL)
	 * @see org.eclipse.mdht.uml.hl7.datatypes.DatatypesPackage#getIVL_REAL_Width()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	REAL getWidth();

	/**
	 * Sets the value of the '{@link org.eclipse.mdht.uml.hl7.datatypes.IVL_REAL#getWidth <em>Width</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Width</em>' containment reference.
	 * @see #getWidth()
	 * @generated
	 */
	void setWidth(REAL value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @param diagnostics The chain of diagnostics to which problems are to be appended.
	 * @param context The cache of context-specific information.
	 * <!-- end-model-doc -->
	 * @model annotation="http://www.eclipse.org/uml2/1.1.0/GenModel body='not self.low.oclIsUndefined() implies self.center.oclIsUndefined() and (self.width.oclIsUndefined() or self.high.oclIsUndefined())'"
	 * @generated
	 */
	boolean validateOptionsContainingLow(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @param diagnostics The chain of diagnostics to which problems are to be appended.
	 * @param context The cache of context-specific information.
	 * <!-- end-model-doc -->
	 * @model annotation="http://www.eclipse.org/uml2/1.1.0/GenModel body='not self.center.oclIsUndefined() implies self.high.oclIsUndefined() and self.low.oclIsUndefined()'"
	 * @generated
	 */
	boolean validateOptionsContainingCenter(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @param diagnostics The chain of diagnostics to which problems are to be appended.
	 * @param context The cache of context-specific information.
	 * <!-- end-model-doc -->
	 * @model annotation="http://www.eclipse.org/uml2/1.1.0/GenModel body='not self.high.oclIsUndefined() implies (self.low.oclIsUndefined() and self.center.oclIsUndefined() and self.width.oclIsUndefined()) or ((not self.low.oclIsUndefined()) and self.width.oclIsUndefined() and self.center.oclIsUndefined()) or ((not self.width.oclIsUndefined()) and self.low.oclIsUndefined() and self.center.oclIsUndefined())'"
	 * @generated
	 */
	boolean validateOptionsContainingHigh(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @param diagnostics The chain of diagnostics to which problems are to be appended.
	 * @param context The cache of context-specific information.
	 * <!-- end-model-doc -->
	 * @model annotation="http://www.eclipse.org/uml2/1.1.0/GenModel body='not self.width.oclIsUndefined() implies ((not self.low.oclIsUndefined()) and self.center.oclIsUndefined() and self.high.oclIsUndefined()) or (self.low.oclIsUndefined() and self.center.oclIsUndefined()) or ((not self.center.oclIsUndefined()) and self.low.oclIsUndefined() and self.high.oclIsUndefined())'"
	 * @generated
	 */
	boolean validateOptionsContainingWidth(DiagnosticChain diagnostics, Map<Object, Object> context);

} // IVL_REAL
