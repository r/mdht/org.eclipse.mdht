/**
 */
package org.eclipse.mdht.uml.hl7.datatypes.impl;

import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.DiagnosticChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.mdht.uml.hl7.datatypes.DatatypesPackage;
import org.eclipse.mdht.uml.hl7.datatypes.IVL_REAL;
import org.eclipse.mdht.uml.hl7.datatypes.IVXB_REAL;
import org.eclipse.mdht.uml.hl7.datatypes.REAL;

import org.eclipse.mdht.uml.hl7.datatypes.operations.IVL_REALOperations;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>IVL REAL</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.mdht.uml.hl7.datatypes.impl.IVL_REALImpl#getLow <em>Low</em>}</li>
 *   <li>{@link org.eclipse.mdht.uml.hl7.datatypes.impl.IVL_REALImpl#getCenter <em>Center</em>}</li>
 *   <li>{@link org.eclipse.mdht.uml.hl7.datatypes.impl.IVL_REALImpl#getHigh <em>High</em>}</li>
 *   <li>{@link org.eclipse.mdht.uml.hl7.datatypes.impl.IVL_REALImpl#getWidth <em>Width</em>}</li>
 * </ul>
 *
 * @generated
 */
public class IVL_REALImpl extends SXCM_REALImpl implements IVL_REAL {
	/**
	 * The cached value of the '{@link #getLow() <em>Low</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLow()
	 * @generated
	 * @ordered
	 */
	protected IVXB_REAL low;

	/**
	 * The cached value of the '{@link #getCenter() <em>Center</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCenter()
	 * @generated
	 * @ordered
	 */
	protected REAL center;

	/**
	 * The cached value of the '{@link #getHigh() <em>High</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHigh()
	 * @generated
	 * @ordered
	 */
	protected IVXB_REAL high;

	/**
	 * The cached value of the '{@link #getWidth() <em>Width</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWidth()
	 * @generated
	 * @ordered
	 */
	protected REAL width;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IVL_REALImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DatatypesPackage.Literals.IVL_REAL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public IVXB_REAL getLow() {
		return low;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLow(IVXB_REAL newLow, NotificationChain msgs) {
		IVXB_REAL oldLow = low;
		low = newLow;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(
				this, Notification.SET, DatatypesPackage.IVL_REAL__LOW, oldLow, newLow);
			if (msgs == null) {
				msgs = notification;
			} else {
				msgs.add(notification);
			}
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLow(IVXB_REAL newLow) {
		if (newLow != low) {
			NotificationChain msgs = null;
			if (low != null) {
				msgs = ((InternalEObject) low).eInverseRemove(
					this, EOPPOSITE_FEATURE_BASE - DatatypesPackage.IVL_REAL__LOW, null, msgs);
			}
			if (newLow != null) {
				msgs = ((InternalEObject) newLow).eInverseAdd(
					this, EOPPOSITE_FEATURE_BASE - DatatypesPackage.IVL_REAL__LOW, null, msgs);
			}
			msgs = basicSetLow(newLow, msgs);
			if (msgs != null) {
				msgs.dispatch();
			}
		} else if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, DatatypesPackage.IVL_REAL__LOW, newLow, newLow));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public REAL getCenter() {
		return center;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCenter(REAL newCenter, NotificationChain msgs) {
		REAL oldCenter = center;
		center = newCenter;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(
				this, Notification.SET, DatatypesPackage.IVL_REAL__CENTER, oldCenter, newCenter);
			if (msgs == null) {
				msgs = notification;
			} else {
				msgs.add(notification);
			}
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCenter(REAL newCenter) {
		if (newCenter != center) {
			NotificationChain msgs = null;
			if (center != null) {
				msgs = ((InternalEObject) center).eInverseRemove(
					this, EOPPOSITE_FEATURE_BASE - DatatypesPackage.IVL_REAL__CENTER, null, msgs);
			}
			if (newCenter != null) {
				msgs = ((InternalEObject) newCenter).eInverseAdd(
					this, EOPPOSITE_FEATURE_BASE - DatatypesPackage.IVL_REAL__CENTER, null, msgs);
			}
			msgs = basicSetCenter(newCenter, msgs);
			if (msgs != null) {
				msgs.dispatch();
			}
		} else if (eNotificationRequired()) {
			eNotify(
				new ENotificationImpl(this, Notification.SET, DatatypesPackage.IVL_REAL__CENTER, newCenter, newCenter));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public IVXB_REAL getHigh() {
		return high;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHigh(IVXB_REAL newHigh, NotificationChain msgs) {
		IVXB_REAL oldHigh = high;
		high = newHigh;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(
				this, Notification.SET, DatatypesPackage.IVL_REAL__HIGH, oldHigh, newHigh);
			if (msgs == null) {
				msgs = notification;
			} else {
				msgs.add(notification);
			}
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setHigh(IVXB_REAL newHigh) {
		if (newHigh != high) {
			NotificationChain msgs = null;
			if (high != null) {
				msgs = ((InternalEObject) high).eInverseRemove(
					this, EOPPOSITE_FEATURE_BASE - DatatypesPackage.IVL_REAL__HIGH, null, msgs);
			}
			if (newHigh != null) {
				msgs = ((InternalEObject) newHigh).eInverseAdd(
					this, EOPPOSITE_FEATURE_BASE - DatatypesPackage.IVL_REAL__HIGH, null, msgs);
			}
			msgs = basicSetHigh(newHigh, msgs);
			if (msgs != null) {
				msgs.dispatch();
			}
		} else if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, DatatypesPackage.IVL_REAL__HIGH, newHigh, newHigh));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public REAL getWidth() {
		return width;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetWidth(REAL newWidth, NotificationChain msgs) {
		REAL oldWidth = width;
		width = newWidth;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(
				this, Notification.SET, DatatypesPackage.IVL_REAL__WIDTH, oldWidth, newWidth);
			if (msgs == null) {
				msgs = notification;
			} else {
				msgs.add(notification);
			}
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setWidth(REAL newWidth) {
		if (newWidth != width) {
			NotificationChain msgs = null;
			if (width != null) {
				msgs = ((InternalEObject) width).eInverseRemove(
					this, EOPPOSITE_FEATURE_BASE - DatatypesPackage.IVL_REAL__WIDTH, null, msgs);
			}
			if (newWidth != null) {
				msgs = ((InternalEObject) newWidth).eInverseAdd(
					this, EOPPOSITE_FEATURE_BASE - DatatypesPackage.IVL_REAL__WIDTH, null, msgs);
			}
			msgs = basicSetWidth(newWidth, msgs);
			if (msgs != null) {
				msgs.dispatch();
			}
		} else if (eNotificationRequired()) {
			eNotify(
				new ENotificationImpl(this, Notification.SET, DatatypesPackage.IVL_REAL__WIDTH, newWidth, newWidth));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOptionsContainingLow(DiagnosticChain diagnostics, Map<Object, Object> context) {
		return IVL_REALOperations.validateOptionsContainingLow(this, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOptionsContainingCenter(DiagnosticChain diagnostics, Map<Object, Object> context) {
		return IVL_REALOperations.validateOptionsContainingCenter(this, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOptionsContainingHigh(DiagnosticChain diagnostics, Map<Object, Object> context) {
		return IVL_REALOperations.validateOptionsContainingHigh(this, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOptionsContainingWidth(DiagnosticChain diagnostics, Map<Object, Object> context) {
		return IVL_REALOperations.validateOptionsContainingWidth(this, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DatatypesPackage.IVL_REAL__LOW:
				return basicSetLow(null, msgs);
			case DatatypesPackage.IVL_REAL__CENTER:
				return basicSetCenter(null, msgs);
			case DatatypesPackage.IVL_REAL__HIGH:
				return basicSetHigh(null, msgs);
			case DatatypesPackage.IVL_REAL__WIDTH:
				return basicSetWidth(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DatatypesPackage.IVL_REAL__LOW:
				return getLow();
			case DatatypesPackage.IVL_REAL__CENTER:
				return getCenter();
			case DatatypesPackage.IVL_REAL__HIGH:
				return getHigh();
			case DatatypesPackage.IVL_REAL__WIDTH:
				return getWidth();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DatatypesPackage.IVL_REAL__LOW:
				setLow((IVXB_REAL) newValue);
				return;
			case DatatypesPackage.IVL_REAL__CENTER:
				setCenter((REAL) newValue);
				return;
			case DatatypesPackage.IVL_REAL__HIGH:
				setHigh((IVXB_REAL) newValue);
				return;
			case DatatypesPackage.IVL_REAL__WIDTH:
				setWidth((REAL) newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DatatypesPackage.IVL_REAL__LOW:
				setLow((IVXB_REAL) null);
				return;
			case DatatypesPackage.IVL_REAL__CENTER:
				setCenter((REAL) null);
				return;
			case DatatypesPackage.IVL_REAL__HIGH:
				setHigh((IVXB_REAL) null);
				return;
			case DatatypesPackage.IVL_REAL__WIDTH:
				setWidth((REAL) null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DatatypesPackage.IVL_REAL__LOW:
				return low != null;
			case DatatypesPackage.IVL_REAL__CENTER:
				return center != null;
			case DatatypesPackage.IVL_REAL__HIGH:
				return high != null;
			case DatatypesPackage.IVL_REAL__WIDTH:
				return width != null;
		}
		return super.eIsSet(featureID);
	}

} // IVL_REALImpl
