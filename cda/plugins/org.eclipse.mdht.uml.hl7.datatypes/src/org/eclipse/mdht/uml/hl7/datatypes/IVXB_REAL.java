/**
 */
package org.eclipse.mdht.uml.hl7.datatypes;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IVXB REAL</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.mdht.uml.hl7.datatypes.IVXB_REAL#getInclusive <em>Inclusive</em>}</li>
 * </ul>
 *
 * @see org.eclipse.mdht.uml.hl7.datatypes.DatatypesPackage#getIVXB_REAL()
 * @model
 * @generated
 */
public interface IVXB_REAL extends REAL {
	/**
	 * Returns the value of the '<em><b>Inclusive</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inclusive</em>' attribute.
	 * @see #setInclusive(Boolean)
	 * @see org.eclipse.mdht.uml.hl7.datatypes.DatatypesPackage#getIVXB_REAL_Inclusive()
	 * @model default="true" ordered="false"
	 * @generated
	 */
	Boolean getInclusive();

	/**
	 * Sets the value of the '{@link org.eclipse.mdht.uml.hl7.datatypes.IVXB_REAL#getInclusive <em>Inclusive</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Inclusive</em>' attribute.
	 * @see #getInclusive()
	 * @generated
	 */
	void setInclusive(Boolean value);

} // IVXB_REAL
