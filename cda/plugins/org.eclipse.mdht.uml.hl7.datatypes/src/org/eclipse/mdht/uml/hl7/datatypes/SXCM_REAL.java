/**
 */
package org.eclipse.mdht.uml.hl7.datatypes;

import org.eclipse.mdht.uml.hl7.vocab.SetOperator;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SXCM REAL</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.mdht.uml.hl7.datatypes.SXCM_REAL#getOperator <em>Operator</em>}</li>
 * </ul>
 *
 * @see org.eclipse.mdht.uml.hl7.datatypes.DatatypesPackage#getSXCM_REAL()
 * @model
 * @generated
 */
public interface SXCM_REAL extends REAL {
	/**
	 * Returns the value of the '<em><b>Operator</b></em>' attribute.
	 * The default value is <code>"I"</code>.
	 * The literals are from the enumeration {@link org.eclipse.mdht.uml.hl7.vocab.SetOperator}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operator</em>' attribute.
	 * @see org.eclipse.mdht.uml.hl7.vocab.SetOperator
	 * @see #setOperator(SetOperator)
	 * @see org.eclipse.mdht.uml.hl7.datatypes.DatatypesPackage#getSXCM_REAL_Operator()
	 * @model default="I" ordered="false"
	 * @generated
	 */
	SetOperator getOperator();

	/**
	 * Sets the value of the '{@link org.eclipse.mdht.uml.hl7.datatypes.SXCM_REAL#getOperator <em>Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operator</em>' attribute.
	 * @see org.eclipse.mdht.uml.hl7.vocab.SetOperator
	 * @see #getOperator()
	 * @generated
	 */
	void setOperator(SetOperator value);

} // SXCM_REAL
