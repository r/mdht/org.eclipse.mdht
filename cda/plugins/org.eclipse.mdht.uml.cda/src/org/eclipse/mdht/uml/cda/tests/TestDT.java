/*******************************************************************************
 * Copyright (c) 2021 seanmuir.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     seanmuir - initial API and implementation
 *
 *******************************************************************************/
package org.eclipse.mdht.uml.cda.tests;

import java.math.BigDecimal;

import org.eclipse.mdht.uml.cda.CDAFactory;
import org.eclipse.mdht.uml.cda.Observation;
import org.eclipse.mdht.uml.cda.util.CDAUtil;
import org.eclipse.mdht.uml.hl7.datatypes.DatatypesFactory;
import org.eclipse.mdht.uml.hl7.datatypes.IVL_TS;
import org.eclipse.mdht.uml.hl7.datatypes.IVXB_TS;
import org.eclipse.mdht.uml.hl7.datatypes.PQ;

/**
 * @author seanmuir
 *
 */
public class TestDT {

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		Observation o = CDAFactory.eINSTANCE.createObservation();

		IVL_TS all = DatatypesFactory.eINSTANCE.createIVL_TS();
		PQ pq = DatatypesFactory.eINSTANCE.createPQ();
		BigDecimal v = BigDecimal.valueOf(100.90);
		pq.setValue(v);

		IVXB_TS high = DatatypesFactory.eINSTANCE.createIVXB_TS();
		high.setValue("2021");

		IVXB_TS low = DatatypesFactory.eINSTANCE.createIVXB_TS();
		low.setValue("2020");

		IVXB_TS center = DatatypesFactory.eINSTANCE.createIVXB_TS();
		center.setValue("2019");

		all.setLow(low);
		all.setCenter(center);
		all.setHigh(high);
		all.setWidth(pq);
		o.setEffectiveTime(all);

		CDAUtil.saveSnippet(o, System.out);

	}

}
