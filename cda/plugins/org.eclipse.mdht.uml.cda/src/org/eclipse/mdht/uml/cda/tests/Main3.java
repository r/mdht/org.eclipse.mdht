/*******************************************************************************
 * Copyright (c) 2021 seanmuir.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     seanmuir - initial API and implementation
 *
 *******************************************************************************/
package org.eclipse.mdht.uml.cda.tests;

import org.eclipse.mdht.uml.cda.CDAFactory;
import org.eclipse.mdht.uml.cda.ClinicalDocument;
import org.eclipse.mdht.uml.cda.Observation;
import org.eclipse.mdht.uml.cda.Participant2;
import org.eclipse.mdht.uml.cda.ParticipantRole;
import org.eclipse.mdht.uml.cda.PlayingEntity;
import org.eclipse.mdht.uml.cda.Section;
import org.eclipse.mdht.uml.cda.util.CDAUtil;
import org.eclipse.mdht.uml.hl7.datatypes.DatatypesFactory;

/**
 * @author seanmuir
 *
 */
public class Main3 {

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		ClinicalDocument cd = CDAFactory.eINSTANCE.createClinicalDocument();
		Section section = CDAFactory.eINSTANCE.createSection();
		Observation observation = CDAFactory.eINSTANCE.createObservation();
		ParticipantRole pr = CDAFactory.eINSTANCE.createParticipantRole();
		PlayingEntity pe = CDAFactory.eINSTANCE.createPlayingEntity();
		pe.setSDTCBirthTime(DatatypesFactory.eINSTANCE.createTS("2021"));
		pr.setPlayingEntity(pe);

		Participant2 p2 = CDAFactory.eINSTANCE.createParticipant2();
		p2.setParticipantRole(pr);

		observation.getParticipants().add(p2);
		// observation.getParticipantRoles().add(pr);
		section.addObservation(observation);
		cd.addSection(section);
		CDAUtil.save(cd, System.out);

	}

}
