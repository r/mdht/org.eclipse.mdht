/*******************************************************************************
 * Copyright (c) 2009, 2011 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.eclipse.mdht.uml.cda.impl;

import java.util.Collection;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.mdht.uml.cda.Birthplace;
import org.eclipse.mdht.uml.cda.CDAPackage;
import org.eclipse.mdht.uml.cda.Guardian;
import org.eclipse.mdht.uml.cda.InfrastructureRootTypeId;
import org.eclipse.mdht.uml.cda.LanguageCommunication;
import org.eclipse.mdht.uml.cda.Patient;
import org.eclipse.mdht.uml.cda.operations.PatientOperations;
import org.eclipse.mdht.uml.hl7.datatypes.BL;
import org.eclipse.mdht.uml.hl7.datatypes.CE;
import org.eclipse.mdht.uml.hl7.datatypes.CS;
import org.eclipse.mdht.uml.hl7.datatypes.ED;
import org.eclipse.mdht.uml.hl7.datatypes.II;
import org.eclipse.mdht.uml.hl7.datatypes.INT;
import org.eclipse.mdht.uml.hl7.datatypes.PN;
import org.eclipse.mdht.uml.hl7.datatypes.TS;
import org.eclipse.mdht.uml.hl7.rim.impl.EntityImpl;
import org.eclipse.mdht.uml.hl7.vocab.EntityClass;
import org.eclipse.mdht.uml.hl7.vocab.EntityDeterminer;
import org.eclipse.mdht.uml.hl7.vocab.NullFlavor;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Patient</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.mdht.uml.cda.impl.PatientImpl#getRealmCodes <em>Realm Code</em>}</li>
 *   <li>{@link org.eclipse.mdht.uml.cda.impl.PatientImpl#getTypeId <em>Type Id</em>}</li>
 *   <li>{@link org.eclipse.mdht.uml.cda.impl.PatientImpl#getTemplateIds <em>Template Id</em>}</li>
 *   <li>{@link org.eclipse.mdht.uml.cda.impl.PatientImpl#getId <em>Id</em>}</li>
 *   <li>{@link org.eclipse.mdht.uml.cda.impl.PatientImpl#getNames <em>Name</em>}</li>
 *   <li>{@link org.eclipse.mdht.uml.cda.impl.PatientImpl#getSDTCDesc <em>SDTC Desc</em>}</li>
 *   <li>{@link org.eclipse.mdht.uml.cda.impl.PatientImpl#getAdministrativeGenderCode <em>Administrative Gender Code</em>}</li>
 *   <li>{@link org.eclipse.mdht.uml.cda.impl.PatientImpl#getBirthTime <em>Birth Time</em>}</li>
 *   <li>{@link org.eclipse.mdht.uml.cda.impl.PatientImpl#getSDTCDeceasedInd <em>SDTC Deceased Ind</em>}</li>
 *   <li>{@link org.eclipse.mdht.uml.cda.impl.PatientImpl#getSDTCDeceasedTime <em>SDTC Deceased Time</em>}</li>
 *   <li>{@link org.eclipse.mdht.uml.cda.impl.PatientImpl#getSDTCMultipleBirthInd <em>SDTC Multiple Birth Ind</em>}</li>
 *   <li>{@link org.eclipse.mdht.uml.cda.impl.PatientImpl#getSDTCMultipleBirthOrderNumber <em>SDTC Multiple Birth Order Number</em>}</li>
 *   <li>{@link org.eclipse.mdht.uml.cda.impl.PatientImpl#getMaritalStatusCode <em>Marital Status Code</em>}</li>
 *   <li>{@link org.eclipse.mdht.uml.cda.impl.PatientImpl#getReligiousAffiliationCode <em>Religious Affiliation Code</em>}</li>
 *   <li>{@link org.eclipse.mdht.uml.cda.impl.PatientImpl#getRaceCode <em>Race Code</em>}</li>
 *   <li>{@link org.eclipse.mdht.uml.cda.impl.PatientImpl#getSDTCRaceCodes <em>SDTC Race Code</em>}</li>
 *   <li>{@link org.eclipse.mdht.uml.cda.impl.PatientImpl#getEthnicGroupCode <em>Ethnic Group Code</em>}</li>
 *   <li>{@link org.eclipse.mdht.uml.cda.impl.PatientImpl#getSDTCEthnicGroupCodes <em>SDTC Ethnic Group Code</em>}</li>
 *   <li>{@link org.eclipse.mdht.uml.cda.impl.PatientImpl#getGuardians <em>Guardian</em>}</li>
 *   <li>{@link org.eclipse.mdht.uml.cda.impl.PatientImpl#getBirthplace <em>Birthplace</em>}</li>
 *   <li>{@link org.eclipse.mdht.uml.cda.impl.PatientImpl#getLanguageCommunications <em>Language Communication</em>}</li>
 *   <li>{@link org.eclipse.mdht.uml.cda.impl.PatientImpl#getNullFlavor <em>Null Flavor</em>}</li>
 *   <li>{@link org.eclipse.mdht.uml.cda.impl.PatientImpl#getClassCode <em>Class Code</em>}</li>
 *   <li>{@link org.eclipse.mdht.uml.cda.impl.PatientImpl#getDeterminerCode <em>Determiner Code</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PatientImpl extends EntityImpl implements Patient {
	/**
	 * The cached value of the '{@link #getRealmCodes() <em>Realm Code</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRealmCodes()
	 * @generated
	 * @ordered
	 */
	protected EList<CS> realmCodes;

	/**
	 * The cached value of the '{@link #getTypeId() <em>Type Id</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypeId()
	 * @generated
	 * @ordered
	 */
	protected InfrastructureRootTypeId typeId;

	/**
	 * The cached value of the '{@link #getTemplateIds() <em>Template Id</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTemplateIds()
	 * @generated
	 * @ordered
	 */
	protected EList<II> templateIds;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected II id;

	/**
	 * The cached value of the '{@link #getNames() <em>Name</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNames()
	 * @generated
	 * @ordered
	 */
	protected EList<PN> names;

	/**
	 * The cached value of the '{@link #getSDTCDesc() <em>SDTC Desc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSDTCDesc()
	 * @generated
	 * @ordered
	 */
	protected ED sDTCDesc;

	/**
	 * The cached value of the '{@link #getAdministrativeGenderCode() <em>Administrative Gender Code</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAdministrativeGenderCode()
	 * @generated
	 * @ordered
	 */
	protected CE administrativeGenderCode;

	/**
	 * The cached value of the '{@link #getBirthTime() <em>Birth Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBirthTime()
	 * @generated
	 * @ordered
	 */
	protected TS birthTime;

	/**
	 * The cached value of the '{@link #getSDTCDeceasedInd() <em>SDTC Deceased Ind</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSDTCDeceasedInd()
	 * @generated
	 * @ordered
	 */
	protected BL sDTCDeceasedInd;

	/**
	 * The cached value of the '{@link #getSDTCDeceasedTime() <em>SDTC Deceased Time</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSDTCDeceasedTime()
	 * @generated
	 * @ordered
	 */
	protected TS sDTCDeceasedTime;

	/**
	 * The cached value of the '{@link #getSDTCMultipleBirthInd() <em>SDTC Multiple Birth Ind</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSDTCMultipleBirthInd()
	 * @generated
	 * @ordered
	 */
	protected BL sDTCMultipleBirthInd;

	/**
	 * The cached value of the '{@link #getSDTCMultipleBirthOrderNumber() <em>SDTC Multiple Birth Order Number</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSDTCMultipleBirthOrderNumber()
	 * @generated
	 * @ordered
	 */
	protected INT sDTCMultipleBirthOrderNumber;

	/**
	 * The cached value of the '{@link #getMaritalStatusCode() <em>Marital Status Code</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaritalStatusCode()
	 * @generated
	 * @ordered
	 */
	protected CE maritalStatusCode;

	/**
	 * The cached value of the '{@link #getReligiousAffiliationCode() <em>Religious Affiliation Code</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReligiousAffiliationCode()
	 * @generated
	 * @ordered
	 */
	protected CE religiousAffiliationCode;

	/**
	 * The cached value of the '{@link #getRaceCode() <em>Race Code</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRaceCode()
	 * @generated
	 * @ordered
	 */
	protected CE raceCode;

	/**
	 * The cached value of the '{@link #getSDTCRaceCodes() <em>SDTC Race Code</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSDTCRaceCodes()
	 * @generated
	 * @ordered
	 */
	protected EList<CE> sDTCRaceCodes;

	/**
	 * The cached value of the '{@link #getEthnicGroupCode() <em>Ethnic Group Code</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEthnicGroupCode()
	 * @generated
	 * @ordered
	 */
	protected CE ethnicGroupCode;

	/**
	 * The cached value of the '{@link #getSDTCEthnicGroupCodes() <em>SDTC Ethnic Group Code</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSDTCEthnicGroupCodes()
	 * @generated
	 * @ordered
	 */
	protected EList<CE> sDTCEthnicGroupCodes;

	/**
	 * The cached value of the '{@link #getGuardians() <em>Guardian</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGuardians()
	 * @generated
	 * @ordered
	 */
	protected EList<Guardian> guardians;

	/**
	 * The cached value of the '{@link #getBirthplace() <em>Birthplace</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBirthplace()
	 * @generated
	 * @ordered
	 */
	protected Birthplace birthplace;

	/**
	 * The cached value of the '{@link #getLanguageCommunications() <em>Language Communication</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLanguageCommunications()
	 * @generated
	 * @ordered
	 */
	protected EList<LanguageCommunication> languageCommunications;

	/**
	 * The default value of the '{@link #getNullFlavor() <em>Null Flavor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNullFlavor()
	 * @generated
	 * @ordered
	 */
	protected static final NullFlavor NULL_FLAVOR_EDEFAULT = NullFlavor.ASKU;

	/**
	 * The cached value of the '{@link #getNullFlavor() <em>Null Flavor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNullFlavor()
	 * @generated
	 * @ordered
	 */
	protected NullFlavor nullFlavor = NULL_FLAVOR_EDEFAULT;

	/**
	 * This is true if the Null Flavor attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean nullFlavorESet;

	/**
	 * The default value of the '{@link #getClassCode() <em>Class Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassCode()
	 * @generated
	 * @ordered
	 */
	protected static final EntityClass CLASS_CODE_EDEFAULT = EntityClass.PSN;

	/**
	 * The cached value of the '{@link #getClassCode() <em>Class Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassCode()
	 * @generated
	 * @ordered
	 */
	protected EntityClass classCode = CLASS_CODE_EDEFAULT;

	/**
	 * This is true if the Class Code attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean classCodeESet;

	/**
	 * The default value of the '{@link #getDeterminerCode() <em>Determiner Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDeterminerCode()
	 * @generated
	 * @ordered
	 */
	protected static final EntityDeterminer DETERMINER_CODE_EDEFAULT = EntityDeterminer.INSTANCE;

	/**
	 * The cached value of the '{@link #getDeterminerCode() <em>Determiner Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDeterminerCode()
	 * @generated
	 * @ordered
	 */
	protected EntityDeterminer determinerCode = DETERMINER_CODE_EDEFAULT;

	/**
	 * This is true if the Determiner Code attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean determinerCodeESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PatientImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CDAPackage.Literals.PATIENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<CS> getRealmCodes() {
		if (realmCodes == null) {
			realmCodes = new EObjectContainmentEList<>(CS.class, this, CDAPackage.PATIENT__REALM_CODE);
		}
		return realmCodes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public InfrastructureRootTypeId getTypeId() {
		return typeId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTypeId(InfrastructureRootTypeId newTypeId, NotificationChain msgs) {
		InfrastructureRootTypeId oldTypeId = typeId;
		typeId = newTypeId;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(
				this, Notification.SET, CDAPackage.PATIENT__TYPE_ID, oldTypeId, newTypeId);
			if (msgs == null) {
				msgs = notification;
			} else {
				msgs.add(notification);
			}
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTypeId(InfrastructureRootTypeId newTypeId) {
		if (newTypeId != typeId) {
			NotificationChain msgs = null;
			if (typeId != null) {
				msgs = ((InternalEObject) typeId).eInverseRemove(
					this, EOPPOSITE_FEATURE_BASE - CDAPackage.PATIENT__TYPE_ID, null, msgs);
			}
			if (newTypeId != null) {
				msgs = ((InternalEObject) newTypeId).eInverseAdd(
					this, EOPPOSITE_FEATURE_BASE - CDAPackage.PATIENT__TYPE_ID, null, msgs);
			}
			msgs = basicSetTypeId(newTypeId, msgs);
			if (msgs != null) {
				msgs.dispatch();
			}
		} else if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, CDAPackage.PATIENT__TYPE_ID, newTypeId, newTypeId));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<II> getTemplateIds() {
		if (templateIds == null) {
			templateIds = new EObjectContainmentEList<>(II.class, this, CDAPackage.PATIENT__TEMPLATE_ID);
		}
		return templateIds;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public II getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetId(II newId, NotificationChain msgs) {
		II oldId = id;
		id = newId;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(
				this, Notification.SET, CDAPackage.PATIENT__ID, oldId, newId);
			if (msgs == null) {
				msgs = notification;
			} else {
				msgs.add(notification);
			}
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setId(II newId) {
		if (newId != id) {
			NotificationChain msgs = null;
			if (id != null) {
				msgs = ((InternalEObject) id).eInverseRemove(
					this, EOPPOSITE_FEATURE_BASE - CDAPackage.PATIENT__ID, null, msgs);
			}
			if (newId != null) {
				msgs = ((InternalEObject) newId).eInverseAdd(
					this, EOPPOSITE_FEATURE_BASE - CDAPackage.PATIENT__ID, null, msgs);
			}
			msgs = basicSetId(newId, msgs);
			if (msgs != null) {
				msgs.dispatch();
			}
		} else if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, CDAPackage.PATIENT__ID, newId, newId));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<PN> getNames() {
		if (names == null) {
			names = new EObjectContainmentEList<>(PN.class, this, CDAPackage.PATIENT__NAME);
		}
		return names;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ED getSDTCDesc() {
		return sDTCDesc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSDTCDesc(ED newSDTCDesc, NotificationChain msgs) {
		ED oldSDTCDesc = sDTCDesc;
		sDTCDesc = newSDTCDesc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(
				this, Notification.SET, CDAPackage.PATIENT__SDTC_DESC, oldSDTCDesc, newSDTCDesc);
			if (msgs == null) {
				msgs = notification;
			} else {
				msgs.add(notification);
			}
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSDTCDesc(ED newSDTCDesc) {
		if (newSDTCDesc != sDTCDesc) {
			NotificationChain msgs = null;
			if (sDTCDesc != null) {
				msgs = ((InternalEObject) sDTCDesc).eInverseRemove(
					this, EOPPOSITE_FEATURE_BASE - CDAPackage.PATIENT__SDTC_DESC, null, msgs);
			}
			if (newSDTCDesc != null) {
				msgs = ((InternalEObject) newSDTCDesc).eInverseAdd(
					this, EOPPOSITE_FEATURE_BASE - CDAPackage.PATIENT__SDTC_DESC, null, msgs);
			}
			msgs = basicSetSDTCDesc(newSDTCDesc, msgs);
			if (msgs != null) {
				msgs.dispatch();
			}
		} else if (eNotificationRequired()) {
			eNotify(
				new ENotificationImpl(this, Notification.SET, CDAPackage.PATIENT__SDTC_DESC, newSDTCDesc, newSDTCDesc));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CE getAdministrativeGenderCode() {
		return administrativeGenderCode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAdministrativeGenderCode(CE newAdministrativeGenderCode, NotificationChain msgs) {
		CE oldAdministrativeGenderCode = administrativeGenderCode;
		administrativeGenderCode = newAdministrativeGenderCode;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(
				this, Notification.SET, CDAPackage.PATIENT__ADMINISTRATIVE_GENDER_CODE, oldAdministrativeGenderCode,
				newAdministrativeGenderCode);
			if (msgs == null) {
				msgs = notification;
			} else {
				msgs.add(notification);
			}
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAdministrativeGenderCode(CE newAdministrativeGenderCode) {
		if (newAdministrativeGenderCode != administrativeGenderCode) {
			NotificationChain msgs = null;
			if (administrativeGenderCode != null) {
				msgs = ((InternalEObject) administrativeGenderCode).eInverseRemove(
					this, EOPPOSITE_FEATURE_BASE - CDAPackage.PATIENT__ADMINISTRATIVE_GENDER_CODE, null, msgs);
			}
			if (newAdministrativeGenderCode != null) {
				msgs = ((InternalEObject) newAdministrativeGenderCode).eInverseAdd(
					this, EOPPOSITE_FEATURE_BASE - CDAPackage.PATIENT__ADMINISTRATIVE_GENDER_CODE, null, msgs);
			}
			msgs = basicSetAdministrativeGenderCode(newAdministrativeGenderCode, msgs);
			if (msgs != null) {
				msgs.dispatch();
			}
		} else if (eNotificationRequired()) {
			eNotify(
				new ENotificationImpl(
					this, Notification.SET, CDAPackage.PATIENT__ADMINISTRATIVE_GENDER_CODE, newAdministrativeGenderCode,
					newAdministrativeGenderCode));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TS getBirthTime() {
		return birthTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBirthTime(TS newBirthTime, NotificationChain msgs) {
		TS oldBirthTime = birthTime;
		birthTime = newBirthTime;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(
				this, Notification.SET, CDAPackage.PATIENT__BIRTH_TIME, oldBirthTime, newBirthTime);
			if (msgs == null) {
				msgs = notification;
			} else {
				msgs.add(notification);
			}
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBirthTime(TS newBirthTime) {
		if (newBirthTime != birthTime) {
			NotificationChain msgs = null;
			if (birthTime != null) {
				msgs = ((InternalEObject) birthTime).eInverseRemove(
					this, EOPPOSITE_FEATURE_BASE - CDAPackage.PATIENT__BIRTH_TIME, null, msgs);
			}
			if (newBirthTime != null) {
				msgs = ((InternalEObject) newBirthTime).eInverseAdd(
					this, EOPPOSITE_FEATURE_BASE - CDAPackage.PATIENT__BIRTH_TIME, null, msgs);
			}
			msgs = basicSetBirthTime(newBirthTime, msgs);
			if (msgs != null) {
				msgs.dispatch();
			}
		} else if (eNotificationRequired()) {
			eNotify(
				new ENotificationImpl(
					this, Notification.SET, CDAPackage.PATIENT__BIRTH_TIME, newBirthTime, newBirthTime));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public BL getSDTCDeceasedInd() {
		return sDTCDeceasedInd;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSDTCDeceasedInd(BL newSDTCDeceasedInd, NotificationChain msgs) {
		BL oldSDTCDeceasedInd = sDTCDeceasedInd;
		sDTCDeceasedInd = newSDTCDeceasedInd;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(
				this, Notification.SET, CDAPackage.PATIENT__SDTC_DECEASED_IND, oldSDTCDeceasedInd, newSDTCDeceasedInd);
			if (msgs == null) {
				msgs = notification;
			} else {
				msgs.add(notification);
			}
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSDTCDeceasedInd(BL newSDTCDeceasedInd) {
		if (newSDTCDeceasedInd != sDTCDeceasedInd) {
			NotificationChain msgs = null;
			if (sDTCDeceasedInd != null) {
				msgs = ((InternalEObject) sDTCDeceasedInd).eInverseRemove(
					this, EOPPOSITE_FEATURE_BASE - CDAPackage.PATIENT__SDTC_DECEASED_IND, null, msgs);
			}
			if (newSDTCDeceasedInd != null) {
				msgs = ((InternalEObject) newSDTCDeceasedInd).eInverseAdd(
					this, EOPPOSITE_FEATURE_BASE - CDAPackage.PATIENT__SDTC_DECEASED_IND, null, msgs);
			}
			msgs = basicSetSDTCDeceasedInd(newSDTCDeceasedInd, msgs);
			if (msgs != null) {
				msgs.dispatch();
			}
		} else if (eNotificationRequired()) {
			eNotify(
				new ENotificationImpl(
					this, Notification.SET, CDAPackage.PATIENT__SDTC_DECEASED_IND, newSDTCDeceasedInd,
					newSDTCDeceasedInd));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TS getSDTCDeceasedTime() {
		return sDTCDeceasedTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSDTCDeceasedTime(TS newSDTCDeceasedTime, NotificationChain msgs) {
		TS oldSDTCDeceasedTime = sDTCDeceasedTime;
		sDTCDeceasedTime = newSDTCDeceasedTime;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(
				this, Notification.SET, CDAPackage.PATIENT__SDTC_DECEASED_TIME, oldSDTCDeceasedTime,
				newSDTCDeceasedTime);
			if (msgs == null) {
				msgs = notification;
			} else {
				msgs.add(notification);
			}
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSDTCDeceasedTime(TS newSDTCDeceasedTime) {
		if (newSDTCDeceasedTime != sDTCDeceasedTime) {
			NotificationChain msgs = null;
			if (sDTCDeceasedTime != null) {
				msgs = ((InternalEObject) sDTCDeceasedTime).eInverseRemove(
					this, EOPPOSITE_FEATURE_BASE - CDAPackage.PATIENT__SDTC_DECEASED_TIME, null, msgs);
			}
			if (newSDTCDeceasedTime != null) {
				msgs = ((InternalEObject) newSDTCDeceasedTime).eInverseAdd(
					this, EOPPOSITE_FEATURE_BASE - CDAPackage.PATIENT__SDTC_DECEASED_TIME, null, msgs);
			}
			msgs = basicSetSDTCDeceasedTime(newSDTCDeceasedTime, msgs);
			if (msgs != null) {
				msgs.dispatch();
			}
		} else if (eNotificationRequired()) {
			eNotify(
				new ENotificationImpl(
					this, Notification.SET, CDAPackage.PATIENT__SDTC_DECEASED_TIME, newSDTCDeceasedTime,
					newSDTCDeceasedTime));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public BL getSDTCMultipleBirthInd() {
		return sDTCMultipleBirthInd;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSDTCMultipleBirthInd(BL newSDTCMultipleBirthInd, NotificationChain msgs) {
		BL oldSDTCMultipleBirthInd = sDTCMultipleBirthInd;
		sDTCMultipleBirthInd = newSDTCMultipleBirthInd;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(
				this, Notification.SET, CDAPackage.PATIENT__SDTC_MULTIPLE_BIRTH_IND, oldSDTCMultipleBirthInd,
				newSDTCMultipleBirthInd);
			if (msgs == null) {
				msgs = notification;
			} else {
				msgs.add(notification);
			}
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSDTCMultipleBirthInd(BL newSDTCMultipleBirthInd) {
		if (newSDTCMultipleBirthInd != sDTCMultipleBirthInd) {
			NotificationChain msgs = null;
			if (sDTCMultipleBirthInd != null) {
				msgs = ((InternalEObject) sDTCMultipleBirthInd).eInverseRemove(
					this, EOPPOSITE_FEATURE_BASE - CDAPackage.PATIENT__SDTC_MULTIPLE_BIRTH_IND, null, msgs);
			}
			if (newSDTCMultipleBirthInd != null) {
				msgs = ((InternalEObject) newSDTCMultipleBirthInd).eInverseAdd(
					this, EOPPOSITE_FEATURE_BASE - CDAPackage.PATIENT__SDTC_MULTIPLE_BIRTH_IND, null, msgs);
			}
			msgs = basicSetSDTCMultipleBirthInd(newSDTCMultipleBirthInd, msgs);
			if (msgs != null) {
				msgs.dispatch();
			}
		} else if (eNotificationRequired()) {
			eNotify(
				new ENotificationImpl(
					this, Notification.SET, CDAPackage.PATIENT__SDTC_MULTIPLE_BIRTH_IND, newSDTCMultipleBirthInd,
					newSDTCMultipleBirthInd));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public INT getSDTCMultipleBirthOrderNumber() {
		return sDTCMultipleBirthOrderNumber;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSDTCMultipleBirthOrderNumber(INT newSDTCMultipleBirthOrderNumber,
			NotificationChain msgs) {
		INT oldSDTCMultipleBirthOrderNumber = sDTCMultipleBirthOrderNumber;
		sDTCMultipleBirthOrderNumber = newSDTCMultipleBirthOrderNumber;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(
				this, Notification.SET, CDAPackage.PATIENT__SDTC_MULTIPLE_BIRTH_ORDER_NUMBER,
				oldSDTCMultipleBirthOrderNumber, newSDTCMultipleBirthOrderNumber);
			if (msgs == null) {
				msgs = notification;
			} else {
				msgs.add(notification);
			}
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSDTCMultipleBirthOrderNumber(INT newSDTCMultipleBirthOrderNumber) {
		if (newSDTCMultipleBirthOrderNumber != sDTCMultipleBirthOrderNumber) {
			NotificationChain msgs = null;
			if (sDTCMultipleBirthOrderNumber != null) {
				msgs = ((InternalEObject) sDTCMultipleBirthOrderNumber).eInverseRemove(
					this, EOPPOSITE_FEATURE_BASE - CDAPackage.PATIENT__SDTC_MULTIPLE_BIRTH_ORDER_NUMBER, null, msgs);
			}
			if (newSDTCMultipleBirthOrderNumber != null) {
				msgs = ((InternalEObject) newSDTCMultipleBirthOrderNumber).eInverseAdd(
					this, EOPPOSITE_FEATURE_BASE - CDAPackage.PATIENT__SDTC_MULTIPLE_BIRTH_ORDER_NUMBER, null, msgs);
			}
			msgs = basicSetSDTCMultipleBirthOrderNumber(newSDTCMultipleBirthOrderNumber, msgs);
			if (msgs != null) {
				msgs.dispatch();
			}
		} else if (eNotificationRequired()) {
			eNotify(
				new ENotificationImpl(
					this, Notification.SET, CDAPackage.PATIENT__SDTC_MULTIPLE_BIRTH_ORDER_NUMBER,
					newSDTCMultipleBirthOrderNumber, newSDTCMultipleBirthOrderNumber));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CE getMaritalStatusCode() {
		return maritalStatusCode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMaritalStatusCode(CE newMaritalStatusCode, NotificationChain msgs) {
		CE oldMaritalStatusCode = maritalStatusCode;
		maritalStatusCode = newMaritalStatusCode;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(
				this, Notification.SET, CDAPackage.PATIENT__MARITAL_STATUS_CODE, oldMaritalStatusCode,
				newMaritalStatusCode);
			if (msgs == null) {
				msgs = notification;
			} else {
				msgs.add(notification);
			}
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMaritalStatusCode(CE newMaritalStatusCode) {
		if (newMaritalStatusCode != maritalStatusCode) {
			NotificationChain msgs = null;
			if (maritalStatusCode != null) {
				msgs = ((InternalEObject) maritalStatusCode).eInverseRemove(
					this, EOPPOSITE_FEATURE_BASE - CDAPackage.PATIENT__MARITAL_STATUS_CODE, null, msgs);
			}
			if (newMaritalStatusCode != null) {
				msgs = ((InternalEObject) newMaritalStatusCode).eInverseAdd(
					this, EOPPOSITE_FEATURE_BASE - CDAPackage.PATIENT__MARITAL_STATUS_CODE, null, msgs);
			}
			msgs = basicSetMaritalStatusCode(newMaritalStatusCode, msgs);
			if (msgs != null) {
				msgs.dispatch();
			}
		} else if (eNotificationRequired()) {
			eNotify(
				new ENotificationImpl(
					this, Notification.SET, CDAPackage.PATIENT__MARITAL_STATUS_CODE, newMaritalStatusCode,
					newMaritalStatusCode));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CE getReligiousAffiliationCode() {
		return religiousAffiliationCode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetReligiousAffiliationCode(CE newReligiousAffiliationCode, NotificationChain msgs) {
		CE oldReligiousAffiliationCode = religiousAffiliationCode;
		religiousAffiliationCode = newReligiousAffiliationCode;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(
				this, Notification.SET, CDAPackage.PATIENT__RELIGIOUS_AFFILIATION_CODE, oldReligiousAffiliationCode,
				newReligiousAffiliationCode);
			if (msgs == null) {
				msgs = notification;
			} else {
				msgs.add(notification);
			}
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setReligiousAffiliationCode(CE newReligiousAffiliationCode) {
		if (newReligiousAffiliationCode != religiousAffiliationCode) {
			NotificationChain msgs = null;
			if (religiousAffiliationCode != null) {
				msgs = ((InternalEObject) religiousAffiliationCode).eInverseRemove(
					this, EOPPOSITE_FEATURE_BASE - CDAPackage.PATIENT__RELIGIOUS_AFFILIATION_CODE, null, msgs);
			}
			if (newReligiousAffiliationCode != null) {
				msgs = ((InternalEObject) newReligiousAffiliationCode).eInverseAdd(
					this, EOPPOSITE_FEATURE_BASE - CDAPackage.PATIENT__RELIGIOUS_AFFILIATION_CODE, null, msgs);
			}
			msgs = basicSetReligiousAffiliationCode(newReligiousAffiliationCode, msgs);
			if (msgs != null) {
				msgs.dispatch();
			}
		} else if (eNotificationRequired()) {
			eNotify(
				new ENotificationImpl(
					this, Notification.SET, CDAPackage.PATIENT__RELIGIOUS_AFFILIATION_CODE, newReligiousAffiliationCode,
					newReligiousAffiliationCode));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CE getRaceCode() {
		return raceCode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRaceCode(CE newRaceCode, NotificationChain msgs) {
		CE oldRaceCode = raceCode;
		raceCode = newRaceCode;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(
				this, Notification.SET, CDAPackage.PATIENT__RACE_CODE, oldRaceCode, newRaceCode);
			if (msgs == null) {
				msgs = notification;
			} else {
				msgs.add(notification);
			}
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setRaceCode(CE newRaceCode) {
		if (newRaceCode != raceCode) {
			NotificationChain msgs = null;
			if (raceCode != null) {
				msgs = ((InternalEObject) raceCode).eInverseRemove(
					this, EOPPOSITE_FEATURE_BASE - CDAPackage.PATIENT__RACE_CODE, null, msgs);
			}
			if (newRaceCode != null) {
				msgs = ((InternalEObject) newRaceCode).eInverseAdd(
					this, EOPPOSITE_FEATURE_BASE - CDAPackage.PATIENT__RACE_CODE, null, msgs);
			}
			msgs = basicSetRaceCode(newRaceCode, msgs);
			if (msgs != null) {
				msgs.dispatch();
			}
		} else if (eNotificationRequired()) {
			eNotify(
				new ENotificationImpl(this, Notification.SET, CDAPackage.PATIENT__RACE_CODE, newRaceCode, newRaceCode));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<CE> getSDTCRaceCodes() {
		if (sDTCRaceCodes == null) {
			sDTCRaceCodes = new EObjectContainmentEList<>(CE.class, this, CDAPackage.PATIENT__SDTC_RACE_CODE);
		}
		return sDTCRaceCodes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CE getEthnicGroupCode() {
		return ethnicGroupCode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEthnicGroupCode(CE newEthnicGroupCode, NotificationChain msgs) {
		CE oldEthnicGroupCode = ethnicGroupCode;
		ethnicGroupCode = newEthnicGroupCode;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(
				this, Notification.SET, CDAPackage.PATIENT__ETHNIC_GROUP_CODE, oldEthnicGroupCode, newEthnicGroupCode);
			if (msgs == null) {
				msgs = notification;
			} else {
				msgs.add(notification);
			}
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setEthnicGroupCode(CE newEthnicGroupCode) {
		if (newEthnicGroupCode != ethnicGroupCode) {
			NotificationChain msgs = null;
			if (ethnicGroupCode != null) {
				msgs = ((InternalEObject) ethnicGroupCode).eInverseRemove(
					this, EOPPOSITE_FEATURE_BASE - CDAPackage.PATIENT__ETHNIC_GROUP_CODE, null, msgs);
			}
			if (newEthnicGroupCode != null) {
				msgs = ((InternalEObject) newEthnicGroupCode).eInverseAdd(
					this, EOPPOSITE_FEATURE_BASE - CDAPackage.PATIENT__ETHNIC_GROUP_CODE, null, msgs);
			}
			msgs = basicSetEthnicGroupCode(newEthnicGroupCode, msgs);
			if (msgs != null) {
				msgs.dispatch();
			}
		} else if (eNotificationRequired()) {
			eNotify(
				new ENotificationImpl(
					this, Notification.SET, CDAPackage.PATIENT__ETHNIC_GROUP_CODE, newEthnicGroupCode,
					newEthnicGroupCode));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Guardian> getGuardians() {
		if (guardians == null) {
			guardians = new EObjectContainmentEList<>(Guardian.class, this, CDAPackage.PATIENT__GUARDIAN);
		}
		return guardians;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Birthplace getBirthplace() {
		return birthplace;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBirthplace(Birthplace newBirthplace, NotificationChain msgs) {
		Birthplace oldBirthplace = birthplace;
		birthplace = newBirthplace;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(
				this, Notification.SET, CDAPackage.PATIENT__BIRTHPLACE, oldBirthplace, newBirthplace);
			if (msgs == null) {
				msgs = notification;
			} else {
				msgs.add(notification);
			}
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBirthplace(Birthplace newBirthplace) {
		if (newBirthplace != birthplace) {
			NotificationChain msgs = null;
			if (birthplace != null) {
				msgs = ((InternalEObject) birthplace).eInverseRemove(
					this, EOPPOSITE_FEATURE_BASE - CDAPackage.PATIENT__BIRTHPLACE, null, msgs);
			}
			if (newBirthplace != null) {
				msgs = ((InternalEObject) newBirthplace).eInverseAdd(
					this, EOPPOSITE_FEATURE_BASE - CDAPackage.PATIENT__BIRTHPLACE, null, msgs);
			}
			msgs = basicSetBirthplace(newBirthplace, msgs);
			if (msgs != null) {
				msgs.dispatch();
			}
		} else if (eNotificationRequired()) {
			eNotify(
				new ENotificationImpl(
					this, Notification.SET, CDAPackage.PATIENT__BIRTHPLACE, newBirthplace, newBirthplace));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<LanguageCommunication> getLanguageCommunications() {
		if (languageCommunications == null) {
			languageCommunications = new EObjectContainmentEList<>(
				LanguageCommunication.class, this, CDAPackage.PATIENT__LANGUAGE_COMMUNICATION);
		}
		return languageCommunications;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<CE> getSDTCEthnicGroupCodes() {
		if (sDTCEthnicGroupCodes == null) {
			sDTCEthnicGroupCodes = new EObjectContainmentEList<>(
				CE.class, this, CDAPackage.PATIENT__SDTC_ETHNIC_GROUP_CODE);
		}
		return sDTCEthnicGroupCodes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NullFlavor getNullFlavor() {
		return nullFlavor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNullFlavor(NullFlavor newNullFlavor) {
		NullFlavor oldNullFlavor = nullFlavor;
		nullFlavor = newNullFlavor == null
				? NULL_FLAVOR_EDEFAULT
				: newNullFlavor;
		boolean oldNullFlavorESet = nullFlavorESet;
		nullFlavorESet = true;
		if (eNotificationRequired()) {
			eNotify(
				new ENotificationImpl(
					this, Notification.SET, CDAPackage.PATIENT__NULL_FLAVOR, oldNullFlavor, nullFlavor,
					!oldNullFlavorESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetNullFlavor() {
		NullFlavor oldNullFlavor = nullFlavor;
		boolean oldNullFlavorESet = nullFlavorESet;
		nullFlavor = NULL_FLAVOR_EDEFAULT;
		nullFlavorESet = false;
		if (eNotificationRequired()) {
			eNotify(
				new ENotificationImpl(
					this, Notification.UNSET, CDAPackage.PATIENT__NULL_FLAVOR, oldNullFlavor, NULL_FLAVOR_EDEFAULT,
					oldNullFlavorESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetNullFlavor() {
		return nullFlavorESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EntityClass getClassCode() {
		return classCode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setClassCode(EntityClass newClassCode) {
		EntityClass oldClassCode = classCode;
		classCode = newClassCode == null
				? CLASS_CODE_EDEFAULT
				: newClassCode;
		boolean oldClassCodeESet = classCodeESet;
		classCodeESet = true;
		if (eNotificationRequired()) {
			eNotify(
				new ENotificationImpl(
					this, Notification.SET, CDAPackage.PATIENT__CLASS_CODE, oldClassCode, classCode,
					!oldClassCodeESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetClassCode() {
		EntityClass oldClassCode = classCode;
		boolean oldClassCodeESet = classCodeESet;
		classCode = CLASS_CODE_EDEFAULT;
		classCodeESet = false;
		if (eNotificationRequired()) {
			eNotify(
				new ENotificationImpl(
					this, Notification.UNSET, CDAPackage.PATIENT__CLASS_CODE, oldClassCode, CLASS_CODE_EDEFAULT,
					oldClassCodeESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetClassCode() {
		return classCodeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EntityDeterminer getDeterminerCode() {
		return determinerCode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDeterminerCode(EntityDeterminer newDeterminerCode) {
		EntityDeterminer oldDeterminerCode = determinerCode;
		determinerCode = newDeterminerCode == null
				? DETERMINER_CODE_EDEFAULT
				: newDeterminerCode;
		boolean oldDeterminerCodeESet = determinerCodeESet;
		determinerCodeESet = true;
		if (eNotificationRequired()) {
			eNotify(
				new ENotificationImpl(
					this, Notification.SET, CDAPackage.PATIENT__DETERMINER_CODE, oldDeterminerCode, determinerCode,
					!oldDeterminerCodeESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetDeterminerCode() {
		EntityDeterminer oldDeterminerCode = determinerCode;
		boolean oldDeterminerCodeESet = determinerCodeESet;
		determinerCode = DETERMINER_CODE_EDEFAULT;
		determinerCodeESet = false;
		if (eNotificationRequired()) {
			eNotify(
				new ENotificationImpl(
					this, Notification.UNSET, CDAPackage.PATIENT__DETERMINER_CODE, oldDeterminerCode,
					DETERMINER_CODE_EDEFAULT, oldDeterminerCodeESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetDeterminerCode() {
		return determinerCodeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateClassCode(DiagnosticChain diagnostics, Map<Object, Object> context) {
		return PatientOperations.validateClassCode(this, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDeterminerCode(DiagnosticChain diagnostics, Map<Object, Object> context) {
		return PatientOperations.validateDeterminerCode(this, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<II> getIds() {
		return PatientOperations.getIds(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CDAPackage.PATIENT__REALM_CODE:
				return ((InternalEList<?>) getRealmCodes()).basicRemove(otherEnd, msgs);
			case CDAPackage.PATIENT__TYPE_ID:
				return basicSetTypeId(null, msgs);
			case CDAPackage.PATIENT__TEMPLATE_ID:
				return ((InternalEList<?>) getTemplateIds()).basicRemove(otherEnd, msgs);
			case CDAPackage.PATIENT__ID:
				return basicSetId(null, msgs);
			case CDAPackage.PATIENT__NAME:
				return ((InternalEList<?>) getNames()).basicRemove(otherEnd, msgs);
			case CDAPackage.PATIENT__SDTC_DESC:
				return basicSetSDTCDesc(null, msgs);
			case CDAPackage.PATIENT__ADMINISTRATIVE_GENDER_CODE:
				return basicSetAdministrativeGenderCode(null, msgs);
			case CDAPackage.PATIENT__BIRTH_TIME:
				return basicSetBirthTime(null, msgs);
			case CDAPackage.PATIENT__SDTC_DECEASED_IND:
				return basicSetSDTCDeceasedInd(null, msgs);
			case CDAPackage.PATIENT__SDTC_DECEASED_TIME:
				return basicSetSDTCDeceasedTime(null, msgs);
			case CDAPackage.PATIENT__SDTC_MULTIPLE_BIRTH_IND:
				return basicSetSDTCMultipleBirthInd(null, msgs);
			case CDAPackage.PATIENT__SDTC_MULTIPLE_BIRTH_ORDER_NUMBER:
				return basicSetSDTCMultipleBirthOrderNumber(null, msgs);
			case CDAPackage.PATIENT__MARITAL_STATUS_CODE:
				return basicSetMaritalStatusCode(null, msgs);
			case CDAPackage.PATIENT__RELIGIOUS_AFFILIATION_CODE:
				return basicSetReligiousAffiliationCode(null, msgs);
			case CDAPackage.PATIENT__RACE_CODE:
				return basicSetRaceCode(null, msgs);
			case CDAPackage.PATIENT__SDTC_RACE_CODE:
				return ((InternalEList<?>) getSDTCRaceCodes()).basicRemove(otherEnd, msgs);
			case CDAPackage.PATIENT__ETHNIC_GROUP_CODE:
				return basicSetEthnicGroupCode(null, msgs);
			case CDAPackage.PATIENT__SDTC_ETHNIC_GROUP_CODE:
				return ((InternalEList<?>) getSDTCEthnicGroupCodes()).basicRemove(otherEnd, msgs);
			case CDAPackage.PATIENT__GUARDIAN:
				return ((InternalEList<?>) getGuardians()).basicRemove(otherEnd, msgs);
			case CDAPackage.PATIENT__BIRTHPLACE:
				return basicSetBirthplace(null, msgs);
			case CDAPackage.PATIENT__LANGUAGE_COMMUNICATION:
				return ((InternalEList<?>) getLanguageCommunications()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CDAPackage.PATIENT__REALM_CODE:
				return getRealmCodes();
			case CDAPackage.PATIENT__TYPE_ID:
				return getTypeId();
			case CDAPackage.PATIENT__TEMPLATE_ID:
				return getTemplateIds();
			case CDAPackage.PATIENT__ID:
				return getId();
			case CDAPackage.PATIENT__NAME:
				return getNames();
			case CDAPackage.PATIENT__SDTC_DESC:
				return getSDTCDesc();
			case CDAPackage.PATIENT__ADMINISTRATIVE_GENDER_CODE:
				return getAdministrativeGenderCode();
			case CDAPackage.PATIENT__BIRTH_TIME:
				return getBirthTime();
			case CDAPackage.PATIENT__SDTC_DECEASED_IND:
				return getSDTCDeceasedInd();
			case CDAPackage.PATIENT__SDTC_DECEASED_TIME:
				return getSDTCDeceasedTime();
			case CDAPackage.PATIENT__SDTC_MULTIPLE_BIRTH_IND:
				return getSDTCMultipleBirthInd();
			case CDAPackage.PATIENT__SDTC_MULTIPLE_BIRTH_ORDER_NUMBER:
				return getSDTCMultipleBirthOrderNumber();
			case CDAPackage.PATIENT__MARITAL_STATUS_CODE:
				return getMaritalStatusCode();
			case CDAPackage.PATIENT__RELIGIOUS_AFFILIATION_CODE:
				return getReligiousAffiliationCode();
			case CDAPackage.PATIENT__RACE_CODE:
				return getRaceCode();
			case CDAPackage.PATIENT__SDTC_RACE_CODE:
				return getSDTCRaceCodes();
			case CDAPackage.PATIENT__ETHNIC_GROUP_CODE:
				return getEthnicGroupCode();
			case CDAPackage.PATIENT__SDTC_ETHNIC_GROUP_CODE:
				return getSDTCEthnicGroupCodes();
			case CDAPackage.PATIENT__GUARDIAN:
				return getGuardians();
			case CDAPackage.PATIENT__BIRTHPLACE:
				return getBirthplace();
			case CDAPackage.PATIENT__LANGUAGE_COMMUNICATION:
				return getLanguageCommunications();
			case CDAPackage.PATIENT__NULL_FLAVOR:
				return getNullFlavor();
			case CDAPackage.PATIENT__CLASS_CODE:
				return getClassCode();
			case CDAPackage.PATIENT__DETERMINER_CODE:
				return getDeterminerCode();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CDAPackage.PATIENT__REALM_CODE:
				getRealmCodes().clear();
				getRealmCodes().addAll((Collection<? extends CS>) newValue);
				return;
			case CDAPackage.PATIENT__TYPE_ID:
				setTypeId((InfrastructureRootTypeId) newValue);
				return;
			case CDAPackage.PATIENT__TEMPLATE_ID:
				getTemplateIds().clear();
				getTemplateIds().addAll((Collection<? extends II>) newValue);
				return;
			case CDAPackage.PATIENT__ID:
				setId((II) newValue);
				return;
			case CDAPackage.PATIENT__NAME:
				getNames().clear();
				getNames().addAll((Collection<? extends PN>) newValue);
				return;
			case CDAPackage.PATIENT__SDTC_DESC:
				setSDTCDesc((ED) newValue);
				return;
			case CDAPackage.PATIENT__ADMINISTRATIVE_GENDER_CODE:
				setAdministrativeGenderCode((CE) newValue);
				return;
			case CDAPackage.PATIENT__BIRTH_TIME:
				setBirthTime((TS) newValue);
				return;
			case CDAPackage.PATIENT__SDTC_DECEASED_IND:
				setSDTCDeceasedInd((BL) newValue);
				return;
			case CDAPackage.PATIENT__SDTC_DECEASED_TIME:
				setSDTCDeceasedTime((TS) newValue);
				return;
			case CDAPackage.PATIENT__SDTC_MULTIPLE_BIRTH_IND:
				setSDTCMultipleBirthInd((BL) newValue);
				return;
			case CDAPackage.PATIENT__SDTC_MULTIPLE_BIRTH_ORDER_NUMBER:
				setSDTCMultipleBirthOrderNumber((INT) newValue);
				return;
			case CDAPackage.PATIENT__MARITAL_STATUS_CODE:
				setMaritalStatusCode((CE) newValue);
				return;
			case CDAPackage.PATIENT__RELIGIOUS_AFFILIATION_CODE:
				setReligiousAffiliationCode((CE) newValue);
				return;
			case CDAPackage.PATIENT__RACE_CODE:
				setRaceCode((CE) newValue);
				return;
			case CDAPackage.PATIENT__SDTC_RACE_CODE:
				getSDTCRaceCodes().clear();
				getSDTCRaceCodes().addAll((Collection<? extends CE>) newValue);
				return;
			case CDAPackage.PATIENT__ETHNIC_GROUP_CODE:
				setEthnicGroupCode((CE) newValue);
				return;
			case CDAPackage.PATIENT__SDTC_ETHNIC_GROUP_CODE:
				getSDTCEthnicGroupCodes().clear();
				getSDTCEthnicGroupCodes().addAll((Collection<? extends CE>) newValue);
				return;
			case CDAPackage.PATIENT__GUARDIAN:
				getGuardians().clear();
				getGuardians().addAll((Collection<? extends Guardian>) newValue);
				return;
			case CDAPackage.PATIENT__BIRTHPLACE:
				setBirthplace((Birthplace) newValue);
				return;
			case CDAPackage.PATIENT__LANGUAGE_COMMUNICATION:
				getLanguageCommunications().clear();
				getLanguageCommunications().addAll((Collection<? extends LanguageCommunication>) newValue);
				return;
			case CDAPackage.PATIENT__NULL_FLAVOR:
				setNullFlavor((NullFlavor) newValue);
				return;
			case CDAPackage.PATIENT__CLASS_CODE:
				setClassCode((EntityClass) newValue);
				return;
			case CDAPackage.PATIENT__DETERMINER_CODE:
				setDeterminerCode((EntityDeterminer) newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CDAPackage.PATIENT__REALM_CODE:
				getRealmCodes().clear();
				return;
			case CDAPackage.PATIENT__TYPE_ID:
				setTypeId((InfrastructureRootTypeId) null);
				return;
			case CDAPackage.PATIENT__TEMPLATE_ID:
				getTemplateIds().clear();
				return;
			case CDAPackage.PATIENT__ID:
				setId((II) null);
				return;
			case CDAPackage.PATIENT__NAME:
				getNames().clear();
				return;
			case CDAPackage.PATIENT__SDTC_DESC:
				setSDTCDesc((ED) null);
				return;
			case CDAPackage.PATIENT__ADMINISTRATIVE_GENDER_CODE:
				setAdministrativeGenderCode((CE) null);
				return;
			case CDAPackage.PATIENT__BIRTH_TIME:
				setBirthTime((TS) null);
				return;
			case CDAPackage.PATIENT__SDTC_DECEASED_IND:
				setSDTCDeceasedInd((BL) null);
				return;
			case CDAPackage.PATIENT__SDTC_DECEASED_TIME:
				setSDTCDeceasedTime((TS) null);
				return;
			case CDAPackage.PATIENT__SDTC_MULTIPLE_BIRTH_IND:
				setSDTCMultipleBirthInd((BL) null);
				return;
			case CDAPackage.PATIENT__SDTC_MULTIPLE_BIRTH_ORDER_NUMBER:
				setSDTCMultipleBirthOrderNumber((INT) null);
				return;
			case CDAPackage.PATIENT__MARITAL_STATUS_CODE:
				setMaritalStatusCode((CE) null);
				return;
			case CDAPackage.PATIENT__RELIGIOUS_AFFILIATION_CODE:
				setReligiousAffiliationCode((CE) null);
				return;
			case CDAPackage.PATIENT__RACE_CODE:
				setRaceCode((CE) null);
				return;
			case CDAPackage.PATIENT__SDTC_RACE_CODE:
				getSDTCRaceCodes().clear();
				return;
			case CDAPackage.PATIENT__ETHNIC_GROUP_CODE:
				setEthnicGroupCode((CE) null);
				return;
			case CDAPackage.PATIENT__SDTC_ETHNIC_GROUP_CODE:
				getSDTCEthnicGroupCodes().clear();
				return;
			case CDAPackage.PATIENT__GUARDIAN:
				getGuardians().clear();
				return;
			case CDAPackage.PATIENT__BIRTHPLACE:
				setBirthplace((Birthplace) null);
				return;
			case CDAPackage.PATIENT__LANGUAGE_COMMUNICATION:
				getLanguageCommunications().clear();
				return;
			case CDAPackage.PATIENT__NULL_FLAVOR:
				unsetNullFlavor();
				return;
			case CDAPackage.PATIENT__CLASS_CODE:
				unsetClassCode();
				return;
			case CDAPackage.PATIENT__DETERMINER_CODE:
				unsetDeterminerCode();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CDAPackage.PATIENT__REALM_CODE:
				return realmCodes != null && !realmCodes.isEmpty();
			case CDAPackage.PATIENT__TYPE_ID:
				return typeId != null;
			case CDAPackage.PATIENT__TEMPLATE_ID:
				return templateIds != null && !templateIds.isEmpty();
			case CDAPackage.PATIENT__ID:
				return id != null;
			case CDAPackage.PATIENT__NAME:
				return names != null && !names.isEmpty();
			case CDAPackage.PATIENT__SDTC_DESC:
				return sDTCDesc != null;
			case CDAPackage.PATIENT__ADMINISTRATIVE_GENDER_CODE:
				return administrativeGenderCode != null;
			case CDAPackage.PATIENT__BIRTH_TIME:
				return birthTime != null;
			case CDAPackage.PATIENT__SDTC_DECEASED_IND:
				return sDTCDeceasedInd != null;
			case CDAPackage.PATIENT__SDTC_DECEASED_TIME:
				return sDTCDeceasedTime != null;
			case CDAPackage.PATIENT__SDTC_MULTIPLE_BIRTH_IND:
				return sDTCMultipleBirthInd != null;
			case CDAPackage.PATIENT__SDTC_MULTIPLE_BIRTH_ORDER_NUMBER:
				return sDTCMultipleBirthOrderNumber != null;
			case CDAPackage.PATIENT__MARITAL_STATUS_CODE:
				return maritalStatusCode != null;
			case CDAPackage.PATIENT__RELIGIOUS_AFFILIATION_CODE:
				return religiousAffiliationCode != null;
			case CDAPackage.PATIENT__RACE_CODE:
				return raceCode != null;
			case CDAPackage.PATIENT__SDTC_RACE_CODE:
				return sDTCRaceCodes != null && !sDTCRaceCodes.isEmpty();
			case CDAPackage.PATIENT__ETHNIC_GROUP_CODE:
				return ethnicGroupCode != null;
			case CDAPackage.PATIENT__SDTC_ETHNIC_GROUP_CODE:
				return sDTCEthnicGroupCodes != null && !sDTCEthnicGroupCodes.isEmpty();
			case CDAPackage.PATIENT__GUARDIAN:
				return guardians != null && !guardians.isEmpty();
			case CDAPackage.PATIENT__BIRTHPLACE:
				return birthplace != null;
			case CDAPackage.PATIENT__LANGUAGE_COMMUNICATION:
				return languageCommunications != null && !languageCommunications.isEmpty();
			case CDAPackage.PATIENT__NULL_FLAVOR:
				return isSetNullFlavor();
			case CDAPackage.PATIENT__CLASS_CODE:
				return isSetClassCode();
			case CDAPackage.PATIENT__DETERMINER_CODE:
				return isSetDeterminerCode();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) {
			return super.toString();
		}

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (nullFlavor: ");
		if (nullFlavorESet) {
			result.append(nullFlavor);
		} else {
			result.append("<unset>");
		}
		result.append(", classCode: ");
		if (classCodeESet) {
			result.append(classCode);
		} else {
			result.append("<unset>");
		}
		result.append(", determinerCode: ");
		if (determinerCodeESet) {
			result.append(determinerCode);
		} else {
			result.append("<unset>");
		}
		result.append(')');
		return result.toString();
	}

} // PatientImpl
